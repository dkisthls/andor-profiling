
#include "atcore.h"

#include "CmdLineUtil.h"
#include "constants.h"
#include "Timer.h" 
#include "utility.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <ostream>
#include <sstream>

bool verbose(true);
bool debugEnabled(false);                               // Overridden by --debug
AT_BOOL directQueueing(DEFAULT_DIRECTQUEUEING);         // Overridden by --directQueueing

std::vector<std::string> allAoiParams { "Width", "Height", "Left", "Top" };
std::vector<std::string> uniqueAoiParams;               // From command line option --aoiParams
std::vector<std::string> aoiParamsToTest;               // The actual aoi tests
std::vector<int> uniqueGainModes;                       // From command line option --gainMode
std::vector<std::string> gainModesToTest;               // The actual gainModes to run
std::vector<AOI> AOIs;

int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);
int Run( const CamInfo& camInfo );
int profileAoi( const CamInfo& camInfo, const std::string& what );

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
    << "Profiles time it takes to configure the camera for changes to both "
    << "Area-of-Interest and Exposure time." << std::endl << std::endl
    << "Usage: " << progname << " [options]" << std::endl
    << "     --help" << std::endl
    << "       This usage help" << std::endl
    << std::endl
    << "     --debug" << std::endl
    << "       Enable debug output." << std::endl
    << std::endl
    << "     --disableDirectQueueing" << std::endl
    << "       Direct queueing is enabled by default. Use this flag to disable it." << std::endl
    << std::endl
    << "     --aoiParams=<type>{,<type>,<type>,<type>}" << std::endl
    << "       Used to specify select AOI parameters for profiling where <type> is one of:" << std::endl
    << "         Width - AOIWidth is varied, AOIHeight, AOILeft, and AOITop remain constant" << std::endl
    << "         Height - AOIHeight is varied, AOIWidth, AOILeft and AOITop remain constant" << std::endl
    << "         Left - AOILeft is varied, AOIWidth, AOIHeight, and AOITop remain constant" << std::endl
    << "         Top - AOITop is varied, AOIWidth, AOIHeight, and AOILeft remain constant" << std::endl
    << "         (default=Width,Height,Left,Top)" << std::endl
    << std::endl
    << "     --gainMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorGainModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaGainModes[i] << std::endl;
  std::cout << "       (default=All gainModes of camera)\n"
    << std::endl;
}
// End of usage()


//------------------------------ cmdLineError() --------------------------------
//
void cmdLineError(char* progname, std::ostringstream& errMsg)
{
  std::cout << std::endl <<  "*** Error - " << errMsg.str() << " ***" << std::endl;
  usage(progname);
  exit(1);
}
// End of cmdLineError()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "debug" )
        {
          debugEnabled = true;
        }
        else if( option == "disableDirectQueueing" )
        {
          directQueueing = AT_FALSE;
        }
        else if( (cmdSwitchPos = option.find("aoiParams")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            if( option.find("Width", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Width");
            }
            if( option.find("Height", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Height");
            }
            if( option.find("Left", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Left");
            }
            if( option.find("Top", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Top");
            }
          }
          else
          {
            errMsg << "Invalid or missing value in option " << &argv[0][0];
            cmdLineError( progName, errMsg );
          }
        }
        else if( (cmdSwitchPos = option.find("gainMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxGainModes = std::max(balorGainModes.size(), zylaGainModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueGainModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxGainModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << "Invalid or missing value in option " << &argv[0][0];
    cmdLineError( progName, errMsg );
  }

  CamInfo camInfo;
  RETURN_ON_ERROR( OpenAndIdentify( camInfo ) );

  if( !uniqueAoiParams.empty() )
    aoiParamsToTest = uniqueAoiParams;
  else
    aoiParamsToTest = allAoiParams;

  if( !uniqueGainModes.empty() )
  {
    for( int v : uniqueGainModes )
    {
      if( v <= 0 || v > camInfo.getGainModes().size() )
      {
        errMsg << "invalid selected gainMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        gainModesToTest.push_back( camInfo.getGainModes()[v-1] );
    }
  }
  else
    gainModesToTest = camInfo.getGainModes();

  // Summarize test setup and command line options...
  std::cout << "General Test Parameters:" << std::endl
            << "  debugEnabled=" << std::boolalpha << debugEnabled << std::endl
            << "  directQueueing=" << std::boolalpha << ((directQueueing == AT_TRUE) ? true : false) << std::endl;

  std::cout << "  aoiParams=";
  for( const auto m : aoiParamsToTest )
    std::cout << m << " ";
  std::cout << std::endl;

  std::cout << "  gainModesToTest=";
  for( int i=0; i<gainModesToTest.size()-1; i++ )
    std::cout << "\"" << gainModesToTest[i] << "\",";
  std::cout << "\"" << gainModesToTest[gainModesToTest.size()-1] << "\"" << std::endl;

  //   Origin-X, Origin-Y, Size-X,   Size-Y  <-- With respect to Sensor 1,1
  //   (Left)     (Top)    (Width)  (Height)
  AOI fullFrameAOI = camInfo.getFullFrameAOI();
  AOIs.emplace_back( 1, 1, fullFrameAOI.width, fullFrameAOI.height, "Full Sensor" );
  AOIs.emplace_back( 1, 1, fullFrameAOI.width, fullFrameAOI.height*0.25, "Left 1/4   (as viewed from front)");
  AOIs.emplace_back( 1, 1, fullFrameAOI.width, fullFrameAOI.height*0.5,  "Left 1/2   (as viewed from front)");
  AOIs.emplace_back( 1, 1, fullFrameAOI.width, fullFrameAOI.height*0.75, "Left 3/4   (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.height*0.75, fullFrameAOI.width, fullFrameAOI.height*0.25, "Right 1/4  (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.height*0.5,  fullFrameAOI.width, fullFrameAOI.height*0.5,  "Right 1/2  (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.height*0.25, fullFrameAOI.width, fullFrameAOI.height*0.75, "Right 3/4  (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.width*0.75, fullFrameAOI.width*0.25, fullFrameAOI.height, "Top 1/4    (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.width*0.5,  fullFrameAOI.width*0.5,  fullFrameAOI.height, "Top 1/2    (as viewed from front)");
  AOIs.emplace_back( 1, fullFrameAOI.width*0.25, fullFrameAOI.width*0.75, fullFrameAOI.height, "Top 3.4    (as viewed from front)");
  AOIs.emplace_back( 1, 1, fullFrameAOI.width*0.25, fullFrameAOI.height, "Bottom 1/4 (as viewed from front)");
  AOIs.emplace_back( 1, 1, fullFrameAOI.width*0.5,  fullFrameAOI.height, "Bottom 1/2 (as viewed from front)");
  AOIs.emplace_back( 1, 1, fullFrameAOI.width*0.75, fullFrameAOI.height, "Bottom 3/4 (as viewed from front)");

  int err = Run( camInfo );

  if (PAUSEAFTER) {
    std::cout << "Press enter to exit ..." << std::endl;
    getchar();
  }

  WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}
// End of main()


//---------------------------------- Run() -------------------------------------
//
int Run( const CamInfo& camInfo ) 
{
  int err = AT_SUCCESS;
  int passNumber(0);
  Timer totalTestDuration;
  Timer testDuration;
  AT_H H(camInfo.getHandle());

  totalTestDuration.start();

  for( std::string shutterMode : camInfo.getShutterModes() )
  {
    for( std::string gainMode : gainModesToTest )
    {
      passNumber++;
      std::string passDescription = "PASS #" + std::to_string(passNumber) 
        + ": ElectronicShutteringMode=" + shutterMode + ", GainMode=" + gainMode;


      std::cout << std::endl;
      std::cout << "====================================================================================================" 
                << std::endl;
      std::cout << passDescription << std::endl;
      auto timenow =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      std::cout << "Date: " << std::ctime(&timenow); 

      RETURN_ON_ERROR( SetBasicConfiguration(camInfo));
      RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", 
                                                 stringToWcs(shutterMode).c_str()) );

      if( camInfo.getCameraId() == BALOR )
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"GainMode", 
                                                   stringToWcs(gainMode).c_str()) );
      }
      else
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"SimplePreAmpGainControl",
                                                   stringToWcs(gainMode).c_str()) );
      }
      
      for( auto param : aoiParamsToTest )
      {
        std::cout << std::endl
                  << "====================================================================================================" 
                  << std::endl;

        testDuration.start();
        err = profileAoi( camInfo, param );
        testDuration.stop();

        if(AT_SUCCESS==err) 
        {
          std::cout.precision(2);
          std::cout << "Completed successfully, test duration=" 
                    << std::fixed << std::setprecision(2)
                    << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;
        }
        else 
          std::cout << "Failed!" << std::endl;

      }

      std::cout << std::endl
                << "====================================================================================================" 
                << std::endl;
    }
  }

  totalTestDuration.stop();
  std::cout.precision(2);
  std::cout << "Testing complete, Total Testing Duration=" 
            << std::fixed << std::setprecision(2)
            << (totalTestDuration.get_dt() / 60.0 ) << "min!" << std::endl << std::endl;

  return err;
}
// End of Run()

int acquireCount=0;

//------------------------------ profileAoi() ----------------------------------
//
int profileAoi( const CamInfo& camInfo, const std::string& what )
{
  AT_64 aoiMin;
  AT_64 aoiMax;
  AT_H H(camInfo.getHandle());

  AOI aoi = camInfo.getFullFrameAOI();

  // Reset to full frame
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

  int *paramToTest;
  if( what == "Width" )
  {
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIWidth", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOIWidth", &aoiMax) );
    paramToTest = &aoi.width;
  }
  else if( what == "Height" )
  {
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIHeight", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOIHeight", &aoiMax) );
    paramToTest = &aoi.height;
  }
  else if( what == "Left" )
  {
    AT_64 tmpMin;
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIWidth", &tmpMin) );

    // Set AOI to minimum width so we can query the full range for AOILeft
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  tmpMin) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOILeft", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOILeft", &aoiMax) );

    // Restore full AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

    paramToTest = &aoi.left;
  }
  else if( what == "Top" )
  {
    AT_64 tmpMin;
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIHeight", &tmpMin) );

    // Set AOI to minimum height so we can query the full range for AOILeft
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", tmpMin) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOITop", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOITop", &aoiMax) );

    // Restore full AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

    paramToTest = &aoi.top;
  }

  std::cout << "AOI " << what << " Min/Max=" << aoiMin << "/" << aoiMax << std::endl;

  // On Width/Height test Left/Top don't change.
  // But, on Left/Top tests, the Width/Height must be adjusted to account for
  // the change in origin.
  *paramToTest = aoiMin;
  aoi.width  = aoi.width - aoi.left + 1;
  aoi.height = aoi.height - aoi.top + 1;
  std::cout << "Starting " << aoi << std::endl;

  *paramToTest = aoiMax;
  aoi.width  = aoi.width - aoi.left + 1;
  aoi.height = aoi.height - aoi.top + 1;
  std::cout << "  Ending " << aoi << std::endl << std::endl;

  // Start acquistion so that in the measurement loop we will be measuring the time
  // to stop, set features, and start.
  std::vector<image_t> frameBuffers;
  AllocateAndQueueBuffers( H, frameBuffers );
  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  Timer tTotal;
  Timer tStop;
  Timer tFlush;
  Timer tConfig;
  Timer tQueue;
  Timer tStart;
  double minStop(10.0),   maxStop(0.0),   sumStop(0.0);
  double minFlush(10.0),  maxFlush(0.0),  sumFlush(0.0);
  double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
  double minQueue(10.0),  maxQueue(0.0),  sumQueue(0.0);
  double minStart(10.0),  maxStart(0.0),  sumStart(0.0);
  double minTotal(10.0),  maxTotal(0.0),  sumTotal(0.0);

  std::ostringstream msgStr;
  msgStr << "Test, Left, Top , Wdth, Hght,   Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  ,  Readout, FrameRate " 
         << std::endl;
  std::cout << msgStr.str() << std::flush;

  int samples(0);
  for( int value=aoiMin; value<=aoiMax; value++ )
//  for( int value=aoiMin; value<=aoiMin + 10; value++ )
  {
    samples++;
    aoi = camInfo.getFullFrameAOI();
    *paramToTest = value;

    tTotal.start();

    tStop.start();
    RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
    tStop.stop();

    tFlush.start();
    RETURN_ON_ERROR(AT_Flush(H));
    frameBuffers.clear();
    tFlush.stop();

    tConfig.start();
    // On Width/Height test Left/Top don't change.
    // But, on Left/Top tests, the Width/Height must be adjusted to account for
    // the change in origin.
    aoi.width  = aoi.width - aoi.left + 1;
    aoi.height = aoi.height - aoi.top + 1;

    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );
    tConfig.stop();

    tQueue.start();
    AllocateAndQueueBuffers( H, frameBuffers );
    tQueue.stop();

    tStart.start();
    RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
    tStart.stop();

    tTotal.stop();

    double readoutTime;
    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime) );
    double frameRate;
    RETURN_ON_ERROR( AT_GetFloat(H, L"FrameRate", &frameRate) );

    double dtStop( tStop.get_dt() );
    double dtFlush( tFlush.get_dt() );
    double dtConfig( tConfig.get_dt() );
    double dtQueue( tQueue.get_dt() );
    double dtStart( tStart.get_dt() );
    double dtTotal( tTotal.get_dt() );
    sumStop   += dtStop;
    sumFlush  += dtFlush;
    sumConfig += dtConfig;
    sumQueue  += dtQueue;
    sumStart  += dtStart;
    sumTotal  += dtTotal;
    minStop   = std::min( minStop, dtStop );
    maxStop   = std::max( maxStop, dtStop );
    minFlush  = std::min( minFlush, dtFlush );
    maxFlush  = std::max( maxFlush, dtFlush );
    minConfig = std::min( minConfig, dtConfig );
    maxConfig = std::max( maxConfig, dtConfig );
    minQueue  = std::min( minQueue, dtQueue );
    maxQueue  = std::max( maxQueue, dtQueue );
    minStart  = std::min( minStart, dtStart );
    maxStart  = std::max( maxStart, dtStart );
    minTotal  = std::min( minTotal, dtTotal );
    maxTotal  = std::max( maxTotal, dtTotal );

    std::ostringstream msgStr;
    msgStr.precision(6);
    msgStr << std::right << std::setw(4) << samples << ", "
           << aoi.toSimpleString() << ", "
           << std::fixed << dtStop << ", "
           << std::fixed << dtFlush << ", "
           << std::fixed << dtConfig << ", "
           << std::fixed << dtQueue << ", "
           << std::fixed << dtStart << ", "
           << std::fixed << dtTotal << ", "
           << std::fixed << readoutTime << ", "
           << std::fixed << frameRate
           << std::endl;
    std::cout << msgStr.str() << std::flush;
  }

  double avgStop(   sumStop / static_cast<double>(samples) ); 
  double avgFlush(  sumFlush / static_cast<double>(samples) ); 
  double avgConfig( sumConfig / static_cast<double>(samples) ); 
  double avgQueue(  sumQueue / static_cast<double>(samples) ); 
  double avgStart(  sumStart / static_cast<double>(samples) ); 
  double avgTotal(  sumTotal / static_cast<double>(samples) ); 

  std::string pad1 = std::string(camInfo.getFullFrameAOI().toSimpleString().length() + 8, ' ');
  std::string pad2 = std::string(camInfo.getFullFrameAOI().toSimpleString().length() + 2, ' ');

  msgStr.str("");
  msgStr.precision(6);
  msgStr 
    << pad1 << "  Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  " << std::endl
    << pad2 << "Min : " << std::fixed << minStop   << ", " << std::fixed << minFlush << ", " 
            << std::fixed << minConfig << ", " << std::fixed << minQueue << ", " 
            << std::fixed << minStart  << ", " << std::fixed << minTotal << std::endl
    << pad2 << "Max : " << std::fixed << maxStop   << ", " << std::fixed << maxFlush << ", " 
            << std::fixed << maxConfig << ", " << std::fixed << maxQueue << ", " 
            << std::fixed << maxStart  << ", " << std::fixed << maxTotal << std::endl
    << pad2 << "Avg : " << std::fixed << avgStop   << ", " << std::fixed << avgFlush << ", " 
            << std::fixed << avgConfig << ", " << std::fixed << avgQueue << ", " 
            << std::fixed << avgStart  << ", " << std::fixed << avgTotal << std::endl;
  std::cout << msgStr.str() << std::flush;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return AT_SUCCESS;
}
// End of profileAoi()


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }


  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  acquireCount++;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
/*      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }
*/

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
//        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;

//        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
        uint16_t* data16 = reinterpret_cast<uint16_t*>(image_data);
        double bytesPerPixel;
        RETURN_ON_ERROR( AT_GetFloat(H, L"BytesPerPixel", &bytesPerPixel) );

        double pixelSum(0);
        int numPixels = image_size / static_cast<int>(bytesPerPixel);
        for( int i=0; i<numPixels; i++)
        {
          pixelSum += data16[i];
        }
        double avgPixel = pixelSum / static_cast<double>(numPixels);
        std::wcout << L"Total Pixels=" << std::setw(8) << std::right << numPixels 
                   << L", Average Pixel Value = " << avgPixel;
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }

    std::wcout<<std::endl;

//    testReadWriteReadOnlyRunning(H, L"During Acquisition");
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}


