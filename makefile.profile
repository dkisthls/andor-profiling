ifeq ($(OS),Windows_NT)
    OS_detected := Windows
else
    OS_detected := $(shell uname -s)
endif

ifeq ($(OS_detected), Windows)
    INCLUDE="../../inc"
	LIBS=-L"../../libs" -latcore
endif
ifeq ($(OS_detected), Linux)
    INCLUDE=/usr/local/inc
	LIBS+= -latcore
endif


srcUtil=$(wildcard ../andor-util/*.cpp)
srcIncl="../andor-util"

$(info $$srcUtil is [${srcUtil}])
$(info $$srcIncl is [${srcIncl}])
$(info $$INCLUDE is [${INCLUDE}])

TARGET_1=profile_Aoi
TARGET_2=profile_AoiExp
TARGET_3=profile_Bin
TARGET_4=profile_BinExp
TARGET_5=profile_Cmd
TARGET_6=profile_Exp

SRC_1=profile_Aoi.cpp $(srcUtil)
SRC_2=profile_AoiExp.cpp $(srcUtil)
SRC_3=profile_Bin.cpp $(srcUtil)
SRC_4=profile_BinExp.cpp $(srcUtil)
SRC_5=profile_Cmd.cpp $(srcUtil)
SRC_6=profile_Exp.cpp $(srcUtil)

VERBOSE = 1

LINK = $(CXX)

CPPFLAGS += -x c++ -O3 -g -c -std=c++11 $(EXTRAFLAGS) -I$(INCLUDE) -I$(srcIncl)

OBJ_DIR=./

OBJS_UTIL = $(srcUtil:%.cpp=%.o) 
OBJS_1 = $(SRC_1:%.cpp=%.o) 
OBJS_2 = $(SRC_2:%.cpp=%.o)
OBJS_3 = $(SRC_3:%.cpp=%.o) 
OBJS_4 = $(SRC_4:%.cpp=%.o) 
OBJS_5 = $(SRC_5:%.cpp=%.o) 
OBJS_6 = $(SRC_6:%.cpp=%.o) 

all: \
     $(TARGET_1) $(TARGET_2) $(TARGET_3) $(TARGET_4) $(TARGET_5) $(TARGET_6)

$(TARGET_1) : $(OBJS_1) 
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_1) $(OBJS_1) $(LIBS)

$(TARGET_2) : $(OBJS_2)
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_2) $(OBJS_2) $(LIBS)

$(TARGET_3) : $(OBJS_3)
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_3) $(OBJS_3) $(LIBS)

$(TARGET_4) : $(OBJS_4)
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_4) $(OBJS_4) $(LIBS)

$(TARGET_5) : $(OBJS_5)
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_5) $(OBJS_5) $(LIBS)

$(TARGET_6) : $(OBJS_6)
	$(LINK) $(EXTRAFLAGS) $(LFLAGS) -o $(TARGET_6) $(OBJS_6) $(LIBS)

%.o: %.cpp
	@echo "Compiling $< ..."
	@if [ $(VERBOSE) -eq 1 ] ; then \
	  echo $(CXX) -o $@ $(CPPFLAGS) $< ; \
	fi ; \
	$(CXX) -o $@ $(CPPFLAGS) $<

clean:
	@echo "Cleaning..."
	@echo "Removing $(OBJS_UTIL)"
	@-rm -rf $(OBJS_UTIL) 
	@echo "Removing $(OBJS_1)"
	@-rm -rf $(OBJS_1) 
	@echo
	@echo "Removing $(OBJS_2)"
	@-rm -rf $(OBJS_2) 
	@echo
	@echo "Removing $(OBJS_3)"
	@-rm -rf $(OBJS_3) 
	@echo
	@echo "Removing $(OBJS_4)"
	@-rm -rf $(OBJS_4)
	@echo
	@echo "Removing $(OBJS_5)"
	@-rm -rf $(OBJS_5)
	@echo
	@echo "Removing $(OBJS_6)"
	@-rm -rf $(OBJS_6)
	@echo
	@echo "Removing $(TARGET_1) $(TARGET_2) $(TARGET_3) $(TARGET_4) $(TARGET_5) $(TARGET_6)"
	@-rm -rf \
		 $(TARGET_1) $(TARGET_2) $(TARGET_3) $(TARGET_4) $(TARGET_5) $(TARGET_6)

