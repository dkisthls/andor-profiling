
#include "atcore.h"

#include "CmdLineUtil.h"
#include "constants.h"
#include "Timer.h" 
#include "utility.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <ostream>
#include <sstream>

// Use this define to skip to a starting AOI parameter value. 
// Useful for debugging.
//#define SKIP_TO 3650

bool verbose(true);
bool shortExp(false);
bool longExp(false);
bool pingPongExp(false);
bool checkExposure(false);                              // Overridden by --checkExposure
bool debugEnabled(false);                               // Overridden by --debug
bool forceError(false);                                 // Overridden by --forceError
AT_BOOL directQueueing(DEFAULT_DIRECTQUEUEING);         // Overridden by --directQueueing
std::string exposureType("short");                      // Overridden by --exposureType

std::vector<std::string> allAoiParams { "Width", "Height", "Left", "Top" };
std::vector<std::string> uniqueAoiParams;       // From command line option --aoiParams
std::vector<std::string> aoiParamsToTest;       // The actual aoi tests
std::vector<int> uniqueGainModes;               // From command line option --gainMode
std::vector<std::string> gainModesToTest;       // The actual gainModes to run
std::vector<int> uniqueShutterModes;            // From command line option --shutterMode
std::vector<std::string> shutterModesToTest;    // The actual shutterModes to run

int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);
int Run( const CamInfo& camInfo );
int profileAoiExp( const CamInfo& camInfo, 
                   const std::string& shutterMode, const std::string& what );

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
    << "Profiles time it takes to configure the camera for changes to both "
    << "Area-of-Interest and Exposure time." << std::endl << std::endl
    << "Usage: " << progname << " [options]" << std::endl
    << "     --help" << std::endl
    << "       This usage help" << std::endl
    << std::endl
    << "     --debug" << std::endl
    << "       Enable debug output." << std::endl
    << std::endl
    << "     --checkExposure" << std::endl
    << "       Check expected vs actual exposure time." << std::endl
    << std::endl
    << "     --disableDirectQueueing" << std::endl
    << "       Direct queueing is enabled by default. Use this flag to disable it." << std::endl
    << std::endl
    << "     --forceError" << std::endl
    << "       BALOR ONLY: Force error condition where exposure time changes if not read when" << std::endl
    << "       acquisition is disabled." << std::endl
    << std::endl
    << "     --exposureType={short | long | pingPong}" << std::endl
    << "       Used to specify the range of exposure times for this test where:" << std::endl
    << "          short - exposure times are < LongExposureTransition boundary" << std::endl
    << "          long - exposure times are >= LongExposureTransition boundary" << std::endl
    << "          pingPong - exposure times alternate between short and long exposures" << std::endl
    << "          (default=short)" << std::endl
    << std::endl
    << "     --aoiParams=<type>{,<type>,<type>,<type>}" << std::endl
    << "       Used to specify select AOI parameters for profiling where <type> is one of:" << std::endl
    << "         Width - AOIWidth is varied, AOIHeight, AOILeft, and AOITop remain constant" << std::endl
    << "         Height - AOIHeight is varied, AOIWidth, AOILeft and AOITop remain constant" << std::endl
    << "         Left - AOILeft is varied, AOIWidth, AOIHeight, and AOITop remain constant" << std::endl
    << "         Top - AOITop is varied, AOIWidth, AOIHeight, and AOILeft remain constant" << std::endl
    << "         (default=Width,Height,Left,Top)" << std::endl
    << std::endl
    << "     --gainMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorGainModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaGainModes[i] << std::endl;
  std::cout << "       (default=All gainModes of camera)\n"
    << std::endl
    << "     --shutterMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorShutterModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaShutterModes[i] << std::endl;
  std::cout << "       (default=All shutterModes of camera)\n"
    << std::endl;
}
// End of usage()


//------------------------------ cmdLineError() --------------------------------
//
void cmdLineError(char* progname, std::ostringstream& errMsg)
{
  std::cout << std::endl <<  "*** Error - " << errMsg.str() << " ***" << std::endl;
  usage(progname);
  exit(1);
}
// End of cmdLineError()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "debug" )
        {
          debugEnabled = true;
        }
        else if( option == "checkExposure" )
        {
          checkExposure = true;
        }
        else if( option == "disableDirectQueueing" )
        {
          directQueueing = AT_FALSE;
        }
        else if( option == "forceError" )
        {
          forceError = true;
        }
        else if( (cmdSwitchPos = option.find("aoiParams")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            if( option.find("Width", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Width");
            }
            if( option.find("Height", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Height");
            }
            if( option.find("Left", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Left");
            }
            if( option.find("Top", equalsSign+1) != std::string::npos )
            {
              auto it = uniqueAoiParams.end();
              uniqueAoiParams.insert(it, "Top");
            }
            bool isValid;
            if( uniqueAoiParams.empty() )
              isValid = false;
            else
            {
              for( std::string uniqueParam : uniqueAoiParams )
              {
                isValid = false;
                for( std::string validParam : allAoiParams )
                {
                  if( uniqueParam == validParam )
                  {
                    isValid = true;
                    break;
                  }
                }
                if( !isValid ) break;
              }
            }
            if( !isValid )
            {
              errMsg << "Invalid or missing value in option " << &argv[0][0];
              cmdLineError( progName, errMsg );
            }
          }
          else
          {
            errMsg << "Invalid or missing value in option " << &argv[0][0];
            cmdLineError( progName, errMsg );
          }
        }
        else if( (cmdSwitchPos = option.find("exposureType")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string value(&argv[0][equalsSign + 3]);
            if( value == "short" || value == "long" || value == "pingPong" )
            {
              exposureType = value;
            }
            else
            {
              errMsg << "Invalid or missing value in option " << &argv[0][0];
              cmdLineError( progName, errMsg );
            }
          }
          else
          {
            errMsg << "Invalid or missing value in option " << &argv[0][0];
            cmdLineError( progName, errMsg );
          }
        }
        else if( (cmdSwitchPos = option.find("gainMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxGainModes = std::max(balorGainModes.size(), zylaGainModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueGainModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxGainModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("shutterMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxShutterModes = std::max(balorShutterModes.size(), zylaShutterModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueShutterModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxShutterModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << "Invalid or missing value in option " << &argv[0][0];
    cmdLineError( progName, errMsg );
  }

  CamInfo camInfo;
  RETURN_ON_ERROR( OpenAndIdentify( camInfo ) );

  if( exposureType == "short" ) 
    shortExp = true;
  else if( exposureType == "long" ) 
    longExp = true;
  else 
    pingPongExp = true;

  if( !uniqueAoiParams.empty() )
    aoiParamsToTest = uniqueAoiParams;
  else
    aoiParamsToTest = allAoiParams;

  if( !uniqueGainModes.empty() )
  {
    for( int v : uniqueGainModes )
    {
      if( v <= 0 || v > camInfo.getGainModes().size() )
      {
        errMsg << "invalid selected gainMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        gainModesToTest.push_back( camInfo.getGainModes()[v-1] );
    }
  }
  else
    gainModesToTest = camInfo.getGainModes();

  if( !uniqueShutterModes.empty() )
  {
    for( int v : uniqueShutterModes )
    {
      if( v <= 0 || v > camInfo.getShutterModes().size() )
      {
        errMsg << "invalid selected shutterMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        shutterModesToTest.push_back( camInfo.getShutterModes()[v-1] );
    }
  }
  else
    shutterModesToTest = camInfo.getShutterModes();

  // Summarize test setup and command line options...
  std::cout << "General Test Parameters:" << std::endl
            << "  debugEnabled=" << std::boolalpha << debugEnabled << std::endl
            << "  checkExposure=" << std::boolalpha << checkExposure << std::endl
            << "  directQueueing=" << std::boolalpha << ((directQueueing == AT_TRUE) ? true : false) << std::endl
            << "  forceError=" << std::boolalpha << forceError << std::endl
            << "  exposureType=" << exposureType << std::endl;

  std::cout << "  aoiParams=";
  for( const auto m : aoiParamsToTest )
    std::cout << m << " ";
  std::cout << std::endl;

  std::cout << "  gainModesToTest=";
  for( int i=0; i<gainModesToTest.size()-1; i++ )
    std::cout << "\"" << gainModesToTest[i] << "\",";
  std::cout << "\"" << gainModesToTest[gainModesToTest.size()-1] << "\"" << std::endl;

  std::cout << "  shutterModesToTest=";
  for( int i=0; i<shutterModesToTest.size()-1; i++ )
    std::cout << "\"" << shutterModesToTest[i] << "\",";
  std::cout << "\"" << shutterModesToTest[shutterModesToTest.size()-1] << "\"" << std::endl;

  int err = Run( camInfo );

  if (PAUSEAFTER) {
    std::cout << "Press enter to exit ..." << std::endl;
    getchar();
  }

  WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}
// End of main()


//---------------------------------- Run() -------------------------------------
//
int Run( const CamInfo& camInfo ) 
{
  int err = AT_SUCCESS;
  int passNumber(0);
  Timer totalTestDuration;
  Timer testDuration;
  AT_H H(camInfo.getHandle());

  totalTestDuration.start();

  for( std::string shutterMode : shutterModesToTest )
  {
    for( std::string gainMode : gainModesToTest )
    {
      passNumber++;
      std::string passDescription = "PASS #" + std::to_string(passNumber) 
        + ": ElectronicShutteringMode=" + shutterMode + ", GainMode=" + gainMode;


      std::cout << std::endl;
      std::cout << "====================================================================================================" 
                << std::endl;
      std::cout << passDescription << std::endl;
      auto timenow =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      std::cout << "Date: " << std::ctime(&timenow);

      RETURN_ON_ERROR( SetBasicConfiguration(camInfo));
      RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", 
                                                 stringToWcs(shutterMode).c_str()) );

      if( camInfo.getCameraId() == BALOR )
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"GainMode", 
                                                   stringToWcs(gainMode).c_str()) );
      }
      else
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"SimplePreAmpGainControl",
                                                   stringToWcs(gainMode).c_str()) );
      }
      
      for( auto param : aoiParamsToTest )
      {

        std::cout << std::endl
                  << "====================================================================================================" 
                  << std::endl;

        testDuration.start();
        err = profileAoiExp( camInfo, shutterMode, param );
        testDuration.stop();

        if(AT_SUCCESS==err) 
        {
          std::cout.precision(2);
          std::cout << "Completed successfully, test duration=" 
                    << std::fixed << std::setprecision(2)
                    << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;
        }
        else 
          std::cout << "Failed, test duration=" 
                    << std::fixed << std::setprecision(2)
                    << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;

      }

      std::cout << std::endl
                << "====================================================================================================" 
                << std::endl;
    }
  }

  totalTestDuration.stop();
  std::cout.precision(2);
  std::cout << "Testing complete, Total Testing Duration=" 
            << std::fixed << std::setprecision(2)
            << (totalTestDuration.get_dt() / 60.0 ) << "min!" << std::endl << std::endl;

  return err;
}
// End of Run()


int acquireCount=0;

//----------------------------- profileAoiExp() --------------------------------
//
int profileAoiExp( 
  const CamInfo& camInfo, 
  const std::string& shutterMode,
  const std::string& what )
{
  double rowReadTime;             // Used to check requested vs actual exposure time
  AT_64 aoiMin;
  AT_64 aoiMax;
  Bin binningOff(1, 1);
  AT_H H(camInfo.getHandle());

  RETURN_ON_ERROR( AT_GetFloat(H, L"RowReadTime", &rowReadTime) );

  AOI aoi = camInfo.getFullFrameAOI();

  // Reset to full frame
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIHBin",   binningOff.x) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIVBin",   binningOff.y) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

  int *paramToTest;
  if( what == "Width" )
  {
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIWidth", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOIWidth", &aoiMax) );

    // Set AOI to somthing close to starting test AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoiMin + 1) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

    paramToTest = &aoi.width;
  }
  else if( what == "Height" )
  {
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIHeight", &aoiMin) );
    if( shortExp ) aoiMin += 2;
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOIHeight", &aoiMax) );

    // Set AOI to somthing close to starting test AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoiMin + 1) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

    paramToTest = &aoi.height;
  }
  else if( what == "Left" )
  {
    AT_64 tmpMin;
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIWidth", &tmpMin) );

    // Set AOI to minimum width so we can query the full range for AOILeft
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  tmpMin) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOILeft", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOILeft", &aoiMax) );

    // Set AOI to somthing close to starting test AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width - 1) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoiMin + 1) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

    paramToTest = &aoi.left;
  }
  else if( what == "Top" )
  {
    AT_64 tmpMin;
    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOIHeight", &tmpMin) );

    // Set AOI to minimum height so we can query the full range for AOILeft
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", tmpMin) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );

    RETURN_ON_ERROR( AT_GetIntMin(H, L"AOITop", &aoiMin) );
    RETURN_ON_ERROR( AT_GetIntMax(H, L"AOITop", &aoiMax) );

    // Set AOI to somthing close to starting test AOI
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height - 1) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoiMin + 1) );

    paramToTest = &aoi.top;
  }

  std::cout << "AOI " << what << " Min/Max=" << aoiMin << "/" << aoiMax << std::endl;

  // On Width/Height test Left/Top don't change.
  // But, on Left/Top tests, the Width/Height must be adjusted to account for
  // the change in origin.
  *paramToTest = aoiMin;
  aoi.width  = aoi.width - aoi.left + 1;
  aoi.height = aoi.height - aoi.top + 1;
  std::cout << "Starting " << aoi << std::endl;

  *paramToTest = aoiMax;
  aoi.width  = aoi.width - aoi.left + 1;
  aoi.height = aoi.height - aoi.top + 1;
  std::cout << "  Ending " << aoi << std::endl;

  // Reset parameter under test to starting minimum value
  *paramToTest = aoiMin;

  // Save current aoi, configured above, as the previous. 
  // When this test is running with a Zyla camera We need the previous AOI in
  // order to calculate the current long exposure transition and decide if 
  // acquisition needs to be disabled in order to set the new exposure time.
  AOI prevAoi( aoi );

  // Start acquisition so that in the measurement loop we will be measuring the time
  // to stop, set features, and start.
  std::vector<image_t> frameBuffers;
  AllocateAndQueueBuffers( H, frameBuffers );

  // Acquisition is disabled. Reading the exposure Min/Max will return the 
  // limits for the camera.
  // NOTE: If acquisition were enabled, then Min/Max would be the limits of the
  //       LongExposureTransition boundary based on what the current exposure time
  //       is set to.
  double expLimits[2];
  RETURN_ON_ERROR(AT_GetFloatMin(H, L"ExposureTime", &expLimits[IDX_MIN]));
  RETURN_ON_ERROR(AT_GetFloatMax(H, L"ExposureTime", &expLimits[IDX_MAX]));

  // Make sure initial exposure time is on the right side of what we're testing and not
  // the value of the first test.
  int rowsRead;
  double let;
  if( shutterMode == "Global" )
  {
    RETURN_ON_ERROR( GetLongExposureTransition(
      camInfo, debugEnabled, prevAoi, let, rowsRead) );
  }
  else
  {
    // This whole test wrt exposure time isn't really applicable for Rolling 
    // Shutter but we'll allow it to run by replacing 'let' with some nominal
    // value.
    RETURN_ON_ERROR(AT_GetFloatMax(H, L"ReadoutTime", &let));
  }


  // Short exposures: Start at min and increment 
  // Long exposures: Balor start at LET and increment
  //                 Zyla start high and decrement
  double expTime[2];
  expTime[SHORT] = expLimits[IDX_MIN];
  expTime[LONG]  = (camInfo.getCameraId() == BALOR) ? let : expLimits[IDX_MAX] - 1.0;

  double expTimeInc[2];
  expTimeInc[SHORT]= 0.00001;
  expTimeInc[LONG] = (camInfo.getCameraId() == BALOR) ? 0.001 : -0.001;

  if( shortExp )
  {
    RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", expTime[SHORT] + rowReadTime) );
  }
  else
  {
    RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", expTime[LONG] + rowReadTime) );
  }
  std::cout << "Initial ";
  RETURN_ON_ERROR( PrintFloatFeature(H, L"ExposureTime", 17) );
  std::cout << std::endl;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  bool skipping(false);
  int skipTo( aoiMin );

#ifdef SKIP_TO
  skipTo = aoiMin + (SKIP_TO - 1); 
  if( skipTo > aoiMin )
  {
    RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
    RETURN_ON_ERROR(AT_Flush(H));
  }
  skipping = true;
  std::cout << "Skipping forward to " << what << "=" << skipTo
            << "; Bypassing AcquisitionStart/Stop, Flush, and Queue while skipping..."
            << std::endl;
#endif

  Timer tTotal;
  Timer tStop;
  Timer tFlush;
  Timer tQueue;
  Timer tStart;
  Timer tCfgAoi;
  Timer tCfgExp;
  double minStop(10.0),   maxStop(0.0),   sumStop(0.0);
  double minFlush(10.0),  maxFlush(0.0),  sumFlush(0.0);
  double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
  double minQueue(10.0),  maxQueue(0.0),  sumQueue(0.0);
  double minStart(10.0),  maxStart(0.0),  sumStart(0.0);
  double minTotal(10.0),  maxTotal(0.0),  sumTotal(0.0);
  double minCfgAoi(10.0), maxCfgAoi(0.0), sumCfgAoi(0.0);
  double minCfgExp(10.0), maxCfgExp(0.0), sumCfgExp(0.0);

  std::ostringstream msgStr;
  msgStr << "Test, Left, Top , Wdth, Hght,  ExpTime ,   Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  ,  Readout, FrameRate  , cfgExp  , cfgAoi " 
         << std::endl;
  std::cout << msgStr.str() << std::flush;

  int testNum(0);
  int samples(0);
  int numExpTimes = 2;
  int errCount(0);

  int expTimeIdx = (longExp ? LONG : SHORT);

  for( int value=aoiMin; value<=aoiMax; value++ )
  {

    // When SKIP_TO is defined we have a target paramter starting value. If we've
    // reached it, set skipping false. This will allow for AcquisitionStart/Stop
    // Flush, and QueueBuffers to occur.
    if( value == skipTo )
    {
      skipping = false;
    }
    
    std::ostringstream stsMsg("");
    testNum++;
    aoi = camInfo.getFullFrameAOI();
    *paramToTest = value;

    int rowsRead;
    double currExpTime;
    RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &currExpTime) );
    RETURN_ON_ERROR( GetLongExposureTransition(
      camInfo, (debugEnabled & !skipping), prevAoi, let, rowsRead) );

    double newExpTime = expTime[expTimeIdx];
    if( shutterMode == "Global" )
    {
      // Use 2*rowReadTime to assure exposure times stay short
      if( (shortExp || (pingPongExp && expTimeIdx == SHORT)) && 
          newExpTime >= (let - (2 * rowReadTime)) )
      {
        newExpTime = expLimits[IDX_MIN];
        expTime[expTimeIdx] = expLimits[IDX_MIN];
      }
    }
    expTime[expTimeIdx] += expTimeInc[expTimeIdx];

    samples++;

    if( debugEnabled && !skipping )
    {
      double tmp1, tmp2, tmp3;
      RETURN_ON_ERROR(AT_GetFloatMin(H, L"ExposureTime", &tmp1));
      RETURN_ON_ERROR(AT_GetFloatMax(H, L"ExposureTime", &tmp2));
      std::cout << std::fixed << std::setprecision(17)
                << "ExposureTime limits with Acquisition Enabled: Min=" << tmp1
                << ", Max=" << std:: fixed << tmp2 << std::endl
                << "LongExposureTransition=" << std::fixed << let << std::endl
                << "  current ExposureTime=" << std::fixed << currExpTime << std::endl
                << "      new ExposureTime=" << std::fixed << newExpTime << std::endl;
    }

    tTotal.start();

    bool stopAcquisitionToChangeExpTime(false);
    if( shutterMode == "Global" )
    {
      if( (currExpTime < let && newExpTime >= (let - rowReadTime)) ||
          (currExpTime >= let && newExpTime < let) )
      {
        stopAcquisitionToChangeExpTime = true;
      }
    }

    if( debugEnabled && !skipping )
    {
      std::cout << "stopAcquisitionToChangeExpTime=" 
                << std::boolalpha << stopAcquisitionToChangeExpTime << std::endl;
    }

    if( !stopAcquisitionToChangeExpTime )
    {
      if( debugEnabled && !skipping )
      {
        double tmp;
        RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &tmp) );
        std::cout << "ExposureTime value before setting newExpTime=" 
                  << std::fixed << tmp << std::endl;
      }
      
      // New exposure time is < or >= long exposure transition. 
      // Exposure can just be set without stopping acquisition
      tCfgExp.start();
      RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", newExpTime) );
      tCfgExp.stop();

      if( debugEnabled && !skipping )
      {
        double tmp;
        RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &tmp) );
        std::cout << "ExposureTime value after setting newExpTime =" 
                  << std::fixed << tmp << std::endl;
      }
    }

    // NOTE: To save time, while skipping is true, bypass AcquistionStopi
    //       and Flush
    if( !skipping )
    {
      tStop.start();
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
      tStop.stop();

      tFlush.start();
      RETURN_ON_ERROR(AT_Flush(H));
      frameBuffers.clear();
      tFlush.stop();
    }

    // On Width/Height test Left/Top don't change.
    // But, on Left/Top tests, the Width/Height must be adjusted to account for
    // the change in origin.
    aoi.width  = aoi.width - aoi.left + 1;
    aoi.height = aoi.height - aoi.top + 1;

    if( stopAcquisitionToChangeExpTime )
    {
      if( debugEnabled && !skipping )
      {
        double tmp1, tmp2, tmp3;
        RETURN_ON_ERROR(AT_GetFloatMin(H, L"ExposureTime", &tmp1));
        RETURN_ON_ERROR(AT_GetFloatMax(H, L"ExposureTime", &tmp2));
        std::cout << std::fixed << std::setprecision(17)
                  << "ExposureTime limits with Acquisition Disabled: Min=" << tmp1
                  << ", Max=" << std:: fixed << tmp2 << std::endl
                  << "LongExposureTransition=" << std::fixed << let << std::endl
                  << "  current ExposureTime=" << std::fixed << currExpTime << std::endl
                  << "      new ExposureTime=" << std::fixed << newExpTime << std::endl;

        double tmp;
        RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &tmp) );
        std::cout << "ExposureTime value before setting newExpTime=" 
                  << std::fixed << tmp << std::endl;
      }

      // New exposure time crosses long exposure transition. Acquisition has 
      // just been stopped so set exposure time now.
      tCfgExp.start();
      RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", newExpTime) );
      tCfgExp.stop();

      if( debugEnabled && !skipping )
      {
        double tmp;
        RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &tmp) );
        std::cout << "ExposureTime value after setting NewExpTime =" 
                  << std::fixed << tmp << std::endl;
      }  
    }

    tCfgAoi.start();
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
    RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );
    tCfgAoi.stop();

    // Save for next text
    prevAoi = aoi;

    // Balor only...
    // The error seen occurs when the exposure time is changed followed by an AOI 
    // change and the exposure time is NOT read. If the exposure time is read 
    // following the change to AOI then the value is expected. Otherwise it is not.
    // The work-around is to read the exposure time while acquisition is disabled 
    // and after setting the AOI.
    // This will need to be done until Andor fixes the issue and all cameas are
    // updated.
    if( camInfo.getCameraId() == BALOR && !forceError )
    {
      double tmp;
      RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &tmp) );
      if( debugEnabled && !skipping )
        std::cout << "ExposureTime value after setting AOI        =" 
                  << std::fixed << tmp << std::endl;
    }

    if( !skipping || (skipping && ((value + 1) == skipTo)) )
    {
      tQueue.start();
      AllocateAndQueueBuffers( H, frameBuffers );
      tQueue.stop();

      tStart.start();
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
      tStart.stop();
    }

    tTotal.stop();

    if( checkExposure || (debugEnabled && !skipping) )
    {
      double actExpTime;
      RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &actExpTime) );

      if( debugEnabled && !skipping )
        std::cout << "ExposureTime value after AcquisitionStart   =" 
                  << std::fixed << std::setprecision(6) << actExpTime 
                  << std::endl;

      if( checkExposure )
      {
        // Verify actual exposure time is within 1 row read time of the requested 
        // value.
        if( actExpTime < (newExpTime - rowReadTime) || 
            actExpTime > (newExpTime + rowReadTime) )
        {
          stsMsg.str("");
          stsMsg << " EXPOSURE TIME ERROR - "
                 << "Expected=" << std::fixed << newExpTime 
                 << "s\u00B1" << std::setprecision(1) << rowReadTime * 1000000.0 << "\u00B5s, "
                 << "Actual=" << std::setprecision(6) << actExpTime << "s!";
          errCount++;
        }
      }
    }

    double readoutTime;
    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime) );
    double frameRate;
    RETURN_ON_ERROR( AT_GetFloat(H, L"FrameRate", &frameRate) );

    double dtStop( tStop.get_dt() );
    double dtFlush( tFlush.get_dt() );
    double dtCfgExp( tCfgExp.get_dt() );
    double dtCfgAoi( tCfgAoi.get_dt() );
    double dtConfig( dtCfgExp + dtCfgAoi );
    double dtQueue( tQueue.get_dt() );
    double dtStart( tStart.get_dt() );
    double dtTotal( tTotal.get_dt() );
    sumStop   += dtStop;
    sumFlush  += dtFlush;
    sumConfig += dtConfig;
    sumQueue  += dtQueue;
    sumStart  += dtStart;
    sumTotal  += dtTotal;
    sumCfgExp += dtCfgExp;
    sumCfgAoi += dtCfgAoi;
    minStop   = std::min( minStop, dtStop );
    maxStop   = std::max( maxStop, dtStop );
    minFlush  = std::min( minFlush, dtFlush );
    maxFlush  = std::max( maxFlush, dtFlush );
    minConfig = std::min( minConfig, dtConfig );
    maxConfig = std::max( maxConfig, dtConfig );
    minQueue  = std::min( minQueue, dtQueue );
    maxQueue  = std::max( maxQueue, dtQueue );
    minStart  = std::min( minStart, dtStart );
    maxStart  = std::max( maxStart, dtStart );
    minTotal  = std::min( minTotal, dtTotal );
    maxTotal  = std::max( maxTotal, dtTotal );
    minCfgExp = std::min( minCfgExp, dtCfgExp );
    maxCfgExp = std::max( maxCfgExp, dtCfgExp );
    minCfgAoi = std::min( minCfgAoi, dtCfgAoi );
    maxCfgAoi = std::max( maxCfgAoi, dtCfgAoi );

    std::ostringstream msgStr;
    msgStr.precision(6);
    msgStr << std::right << std::setw(4) << testNum << ", "
           << aoi.toSimpleString() << ", "
           << std::setfill(' ') << std::setw(9) 
           << std::fixed << std::setprecision(6) << newExpTime << ", "
           << std::fixed << dtStop << ", "
           << std::fixed << dtFlush << ", "
           << std::fixed << dtConfig << ", "
           << std::fixed << dtQueue << ", "
           << std::fixed << dtStart << ", "
           << std::fixed << dtTotal << ", "
           << std::fixed << readoutTime << ", "
           << std::setfill(' ') << std::setw(11) 
           << std::fixed << std::setprecision(6) << std::fixed << frameRate << ", "
           << std::fixed << dtCfgExp << ", "
           << std::fixed << dtCfgAoi << ", "
           << stsMsg.str()
           << std::endl;
    std::cout << msgStr.str() << std::flush;
  
    if( pingPongExp )
      expTimeIdx = (expTimeIdx + 1) % numExpTimes;
  }

  double avgStop(   sumStop / static_cast<double>(samples) ); 
  double avgFlush(  sumFlush / static_cast<double>(samples) ); 
  double avgConfig( sumConfig / static_cast<double>(samples) ); 
  double avgQueue(  sumQueue / static_cast<double>(samples) ); 
  double avgStart(  sumStart / static_cast<double>(samples) ); 
  double avgTotal(  sumTotal / static_cast<double>(samples) ); 
  double avgCfgExp( sumCfgExp / static_cast<double>(samples) ); 
  double avgCfgAoi( sumCfgAoi / static_cast<double>(samples) ); 

  std::string pad1 = std::string(camInfo.getFullFrameAOI().toSimpleString().length() + 8, ' ');
  std::string pad2 = std::string(camInfo.getFullFrameAOI().toSimpleString().length() + 2, ' ');

  msgStr.str("");
  msgStr.precision(6);
  msgStr 
    << pad1 << "  Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  ,  cfgExp ,  cfgAoi" << std::endl
    << pad2 << "Min : " 
            << std::fixed << minStop   << ", " << std::fixed << minFlush << ", " 
            << std::fixed << minConfig << ", " << std::fixed << minQueue << ", " 
            << std::fixed << minStart  << ", " << std::fixed << minTotal << ", "
            << std::fixed << minCfgExp << ", " << std::fixed << minCfgAoi << std::endl
    << pad2 << "Max : " 
            << std::fixed << maxStop   << ", " << std::fixed << maxFlush << ", " 
            << std::fixed << maxConfig << ", " << std::fixed << maxQueue << ", " 
            << std::fixed << maxStart  << ", " << std::fixed << maxTotal << ", "
            << std::fixed << maxCfgExp << ", " << std::fixed << maxCfgAoi << std::endl
    << pad2 << "Avg : " 
            << std::fixed << avgStop   << ", " << std::fixed << avgFlush << ", " 
            << std::fixed << avgConfig << ", " << std::fixed << avgQueue << ", " 
            << std::fixed << avgStart  << ", " << std::fixed << avgTotal << ", "
            << std::fixed << avgCfgExp << ", " << std::fixed << avgCfgAoi << std::endl;
  std::cout << msgStr.str() << std::flush;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  int status = AT_SUCCESS;
  if( checkExposure && errCount > 0 )
  {
    std::cout << "Number of Exposure Time Failures: " << errCount << std::endl;
    status = AT_ERR_OUTOFRANGE;
  }

  return status;
}
// End of profileAoiExp()


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }


  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  acquireCount++;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
/*      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }
*/

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
//        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;

//        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
        uint16_t* data16 = reinterpret_cast<uint16_t*>(image_data);
        double bytesPerPixel;
        RETURN_ON_ERROR( AT_GetFloat(H, L"BytesPerPixel", &bytesPerPixel) );

        double pixelSum(0);
        int numPixels = image_size / static_cast<int>(bytesPerPixel);
        for( int i=0; i<numPixels; i++)
        {
          pixelSum += data16[i];
        }
        double avgPixel = pixelSum / static_cast<double>(numPixels);
        std::wcout << L"Total Pixels=" << std::setw(8) << std::right << numPixels 
                   << L", Average Pixel Value = " << avgPixel;
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }

    std::wcout<<std::endl;

//    testReadWriteReadOnlyRunning(H, L"During Acquisition");
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}


