
#include "atcore.h"

#include "CmdLineUtil.h"
#include "constants.h"
#include "Timer.h" 
#include "utility.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <ostream>
#include <sstream>

bool verbose(true);
bool debugEnabled(false);                               // Overridden by --debug
AT_BOOL directQueueing(DEFAULT_DIRECTQUEUEING);         // Overridden by --directQueueing

// NxN

const std::vector<Bin> Bins = {
  Bin(1,1), Bin(2,2), Bin(1,1), Bin(3,3), Bin(1,1), Bin(4,4), Bin(1,1), Bin(8,8),
  Bin(2,2), Bin(3,3), Bin(2,2), Bin(4,4), Bin(2,2), Bin(8,8),
  Bin(3,3), Bin(4,4), Bin(3,3), Bin(8,8),
  Bin(4,4), Bin(8,8),
  Bin(1,1)
};
/*
// 1xN
const std::vector<Bin> Bins = {
  Bin(1,1), Bin(1,2), Bin(1,1), Bin(1,3), Bin(1,1), Bin(1,4), Bin(1,1), Bin(1,8),
  Bin(1,2), Bin(1,3), Bin(1,2), Bin(1,4), Bin(1,2), Bin(1,8),
  Bin(1,3), Bin(1,4), Bin(1,3), Bin(1,8),
  Bin(1,4), Bin(1,8),
  Bin(1,1)
};

// Nx1
const std::vector<Bin> Bins = {
  Bin(1,1), Bin(2,1), Bin(1,1), Bin(3,1), Bin(1,1), Bin(4,1), Bin(1,1), Bin(8,1),
  Bin(2,1), Bin(3,1), Bin(2,1), Bin(4,1), Bin(2,1), Bin(8,1),
  Bin(3,1), Bin(4,1), Bin(3,1), Bin(8,1),
  Bin(4,1), Bin(8,1),
  Bin(1,1)
};
*/

std::vector<int> uniqueGainModes;          // From command line option --gainMode
std::vector<std::string> gainModesToTest;  // The actual gainModes to run

int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);
int Run( const CamInfo& camInfo );
int profileBin( const CamInfo& camInfo, const std::string& what );

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
    << "Profiles time it takes to configure the camera for binning." << std::endl << std::endl
    << "Usage: " << progname << " [options]" << std::endl
    << "     --help" << std::endl
    << "       This usage help" << std::endl
    << std::endl
    << "     --debug" << std::endl
    << "       Enable debug output." << std::endl
    << std::endl
    << "     --disableDirectQueueing=<bool>\n"
    << "       Direct queueing is enabled by default. Use this flag to disable it.\n"
    << std::endl
    << "     --gainMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorGainModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaGainModes[i] << std::endl;
  std::cout << "       (default=All gainModes of camera)\n"
    << std::endl;
}
// End of usage()


//------------------------------ cmdLineError() --------------------------------
//
void cmdLineError(char* progname, std::ostringstream& errMsg)
{
  std::cout << std::endl <<  "*** Error - " << errMsg.str() << " ***" << std::endl;
  usage(progname);
  exit(1);
}
// End of cmdLineError()

//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "debug" )
        {
          debugEnabled = true;
        }
        else if( option == "disableDirectQueueing" )
        {
          directQueueing = AT_FALSE;
        }
        else if( (cmdSwitchPos = option.find("gainMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxGainModes = std::max(balorGainModes.size(), zylaGainModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueGainModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxGainModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << "Invalid or missing value in option " << &argv[0][0];
    cmdLineError( progName, errMsg );
  }

  CamInfo camInfo;
  RETURN_ON_ERROR( OpenAndIdentify( camInfo ) );

  if( !uniqueGainModes.empty() )
  {
    for( int v : uniqueGainModes )
    {
      if( v <= 0 || v > camInfo.getGainModes().size() )
      {
        errMsg << "invalid selected gainMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        gainModesToTest.push_back( camInfo.getGainModes()[v-1] );
    }
  }
  else
    gainModesToTest = camInfo.getGainModes();

  // Summarize test setup and command line options...
  std::cout << "General Test Parameters:" << std::endl
            << "  debugEnabled=" << std::boolalpha << debugEnabled << std::endl
            << "  directQueueing=" << std::boolalpha << ((directQueueing == AT_TRUE) ? true : false) << std::endl;

  std::cout << "  gainModesToTest=";
  for( int i=0; i<gainModesToTest.size()-1; i++ )
    std::cout << "\"" << gainModesToTest[i] << "\",";
  std::cout << "\"" << gainModesToTest[gainModesToTest.size()-1] << "\"" << std::endl;

  int err = Run( camInfo );

  if (PAUSEAFTER) {
    std::cout << "Press enter to exit ..." << std::endl;
    getchar();
  }

  WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}
// End of main()


//---------------------------------- Run() -------------------------------------
//
int Run( const CamInfo& camInfo ) 
{
  int err = AT_SUCCESS;
  int passNumber(0);
  Timer totalTestDuration;
  Timer testDuration;
  AT_H H(camInfo.getHandle());

  totalTestDuration.start();

  for( std::string shutterMode : camInfo.getShutterModes() )
  {
    for( std::string gainMode : gainModesToTest )
    {
      passNumber++;
      std::string passDescription = "PASS #" + std::to_string(passNumber) 
        + ": ElectronicShutteringMode=" + shutterMode + ", GainMode=" + gainMode;


      std::cout << std::endl;
      std::cout << "====================================================================================================" 
                << std::endl;
      std::cout << passDescription << std::endl;
      auto timenow =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      std::cout << "Date: " << std::ctime(&timenow);

      RETURN_ON_ERROR( SetBasicConfiguration(camInfo));
      RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", 
                                                 stringToWcs(shutterMode).c_str()) );

      if( camInfo.getCameraId() == BALOR )
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"GainMode", 
                                                   stringToWcs(gainMode).c_str()) );
      }
      else
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"SimplePreAmpGainControl",
                                                   stringToWcs(gainMode).c_str()) );
      }

      std::cout << std::endl
                << "====================================================================================================" 
                << std::endl;

      std::string what;
      what = "AOIHBin/AOIVBin";
      testDuration.start();
      err = profileBin( camInfo, what );
      testDuration.stop();

      if(AT_SUCCESS==err) 
      {
        std::cout.precision(2);
        std::cout << "Completed successfully, test duration=" 
                  << std::fixed << std::setprecision(2)
                  << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;
      }
      else 
        std::cout << "Failed!" << std::endl;
/*
      std::cout << std::endl
                << "====================================================================================================" 
                << std::endl;

      what ="AOIBinning";
      testDuration.start();
      err = profileBin( camInfo, what );
      testDuration.stop();

      if(AT_SUCCESS==err) 
      {
        std::cout.precision(2);
        std::cout << "Completed successfully, test duration=" 
                  << std::fixed << std::setprecision(2)
                  << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;
      }
      else 
        std::cout << "Failed!" << std::endl;
*/
      std::cout << std::endl
                << "====================================================================================================" 
                << std::endl;
    }
  }

  totalTestDuration.stop();
  std::cout.precision(2);
  std::cout << "Testing complete, Total Testing Duration=" 
            << std::fixed << std::setprecision(2)
            << (totalTestDuration.get_dt() / 60.0 ) << "min!" << std::endl << std::endl;

  return err;
}
// End of Run()


int acquireCount=0;

//------------------------------ profileBin() ----------------------------------
//
int profileBin( const CamInfo& camInfo, const std::string& what )
{
  AT_64 aoiMin;
  AT_64 aoiMax;
  AT_H H(camInfo.getHandle());
  Bin binningOff(1,1);
  std::ostringstream msgStr;

  // Set Binning to something other than the first entry in the list so that the
  // first test has to actually do something.
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIHBin", Bins[1].x) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIVBin", Bins[1].y) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width/Bins[1].x) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height/Bins[1].y) );
  RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

  // On Width/Height test Left/Top don't change.
  // But, on Left/Top tests, the Width/Height must be adjusted to account for
  // the change in origin.
  msgStr << "Configuring Binning using: " << what << std::endl
         << "Binning Transitions: " << std::endl;
  int count(0);
  if( what == "AOIBinning" )
  {
    for( Bin bin: Bins )
    {
      msgStr << "\"" << bin.x << "x" << bin.y << "\"->";
      if( ++count == 11 )
        msgStr << std::endl;
    }
  }
  else
  {
    for( Bin bin: Bins )
    {
      msgStr << "(" << bin.x << "," << bin.y << ")->";
      if( ++count == 11 )
        msgStr << std::endl;
    }
  }
  std::string tmpStr = msgStr.str();
  auto pos=tmpStr.find_last_of("->");
  tmpStr.erase( pos-1 );
  std::cout << tmpStr << std::endl << std::endl;

  // Start acquistion so that in the measurement loop we will be measuring the time
  // to stop, set features, and start.
  std::vector<image_t> frameBuffers;
  AllocateAndQueueBuffers( H, frameBuffers );
  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  Timer tTotal;
  Timer tStop;
  Timer tFlush;
  Timer tConfig;
  Timer tQueue;
  Timer tStart;
  double minStop(10.0),   maxStop(0.0),   sumStop(0.0);
  double minFlush(10.0),  maxFlush(0.0),  sumFlush(0.0);
  double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
  double minQueue(10.0),  maxQueue(0.0),  sumQueue(0.0);
  double minStart(10.0),  maxStart(0.0),  sumStart(0.0);
  double minTotal(10.0),  maxTotal(0.0),  sumTotal(0.0);

  msgStr.str("");
  msgStr << "Test, BinX, BinY, Left, Top , Wdth, Hght,   Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  ,  Readout, FrameRate" 
         << std::endl;
  std::cout << msgStr.str() << std::flush;

  int samples(0);
  for( Bin bin: Bins )
  {
    samples++;

    // On binning tests Left/Top don't change.
    // AOI width/height are in super-pixels.
    AOI aoi(camInfo.getFullFrameAOI().left,
            camInfo.getFullFrameAOI().top,
            camInfo.getFullFrameAOI().width / bin.x, 
            camInfo.getFullFrameAOI().height / bin.y,
            camInfo.getFullFrameAOI().desc);
    std::wstring aoiBinning(std::to_wstring(bin.x) + L"x" + std::to_wstring(bin.y));

    tTotal.start();

    tStop.start();
    RETURN_ON_ERROR( AT_Command(H, L"AcquisitionStop") );
    tStop.stop();

    tFlush.start();
    RETURN_ON_ERROR( AT_Flush(H) );
    frameBuffers.clear();
    tFlush.stop();

    tConfig.start();
    if( what == "AOIBinning" )    // Use AOIBinning
    {
      // Apply new binning and AOI(in super-pixels).
      RETURN_ON_ERROR( AT_SetEnumString(H, L"AOIBinning", aoiBinning.c_str()) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );
    }
    else                          // Use AOIHBin, AOIVBin
    {
      // Apply new binning and AOI(in super-pixels).
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIHBin",   bin.x) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIVBin",   bin.y) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
      RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );
    }
    tConfig.stop();

    tQueue.start();
    AllocateAndQueueBuffers( H, frameBuffers );
    tQueue.stop();

    tStart.start();
    RETURN_ON_ERROR( AT_Command(H, L"AcquisitionStart") );
    tStart.stop();

    tTotal.stop();

    double readoutTime;
    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime) );
    double frameRate;
    RETURN_ON_ERROR( AT_GetFloat(H, L"FrameRate", &frameRate) );

    double dtStop( tStop.get_dt() );
    double dtFlush( tFlush.get_dt() );
    double dtConfig( tConfig.get_dt() );
    double dtQueue( tQueue.get_dt() );
    double dtStart( tStart.get_dt() );
    double dtTotal( tTotal.get_dt() );
    sumStop   += dtStop;
    sumFlush  += dtFlush;
    sumConfig += dtConfig;
    sumQueue  += dtQueue;
    sumStart  += dtStart;
    sumTotal  += dtTotal;
    minStop   = std::min( minStop, dtStop );
    maxStop   = std::max( maxStop, dtStop );
    minFlush  = std::min( minFlush, dtFlush );
    maxFlush  = std::max( maxFlush, dtFlush );
    minConfig = std::min( minConfig, dtConfig );
    maxConfig = std::max( maxConfig, dtConfig );
    minQueue  = std::min( minQueue, dtQueue );
    maxQueue  = std::max( maxQueue, dtQueue );
    minStart  = std::min( minStart, dtStart );
    maxStart  = std::max( maxStart, dtStart );
    minTotal  = std::min( minTotal, dtTotal );
    maxTotal  = std::max( maxTotal, dtTotal );

    std::ostringstream msgStr;
    msgStr.precision(6);
    msgStr << std::right << std::setw(4) << samples << ", "
           << bin.toSimpleString() << ", "
           << aoi.toSimpleString() << ", "
           << std::fixed << dtStop << ", "
           << std::fixed << dtFlush << ", "
           << std::fixed << dtConfig << ", "
           << std::fixed << dtQueue << ", "
           << std::fixed << dtStart << ", "
           << std::fixed << dtTotal << ", "
           << std::fixed << readoutTime << ", "
           << std::fixed << frameRate
           << std::endl;
    std::cout << msgStr.str() << std::flush;
  }

  double avgStop(   sumStop / static_cast<double>(samples) ); 
  double avgFlush(  sumFlush / static_cast<double>(samples) ); 
  double avgConfig( sumConfig / static_cast<double>(samples) ); 
  double avgQueue(  sumQueue / static_cast<double>(samples) ); 
  double avgStart(  sumStart / static_cast<double>(samples) ); 
  double avgTotal(  sumTotal / static_cast<double>(samples) ); 

  std::string pad1 = std::string(
    binningOff.toSimpleString().length() + 2 + 
    camInfo.getFullFrameAOI().toSimpleString().length() + 8, ' ');
  std::string pad2 = std::string(
    binningOff.toSimpleString().length() + 2 + 
    camInfo.getFullFrameAOI().toSimpleString().length() + 2, ' ');

  msgStr.str("");
  msgStr.precision(6);
  msgStr 
    << pad1 << "  Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  " << std::endl
    << pad2 << "Min : " << std::fixed << minStop   << ", " << std::fixed << minFlush << ", " 
            << std::fixed << minConfig << ", " << std::fixed << minQueue << ", " 
            << std::fixed << minStart  << ", " << std::fixed << minTotal << std::endl
    << pad2 << "Max : " << std::fixed << maxStop   << ", " << std::fixed << maxFlush << ", " 
            << std::fixed << maxConfig << ", " << std::fixed << maxQueue << ", " 
            << std::fixed << maxStart  << ", " << std::fixed << maxTotal << std::endl
    << pad2 << "Avg : " << std::fixed << avgStop   << ", " << std::fixed << avgFlush << ", " 
            << std::fixed << avgConfig << ", " << std::fixed << avgQueue << ", " 
            << std::fixed << avgStart  << ", " << std::fixed << avgTotal << std::endl;
  std::cout << msgStr.str() << std::flush;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return AT_SUCCESS;
}
// End of profileBin()


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }


  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  acquireCount++;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
/*      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }
*/

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
//        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;

//        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
        uint16_t* data16 = reinterpret_cast<uint16_t*>(image_data);
        double bytesPerPixel;
        RETURN_ON_ERROR( AT_GetFloat(H, L"BytesPerPixel", &bytesPerPixel) );

        double pixelSum(0);
        int numPixels = image_size / static_cast<int>(bytesPerPixel);
        for( int i=0; i<numPixels; i++)
        {
          pixelSum += data16[i];
        }
        double avgPixel = pixelSum / static_cast<double>(numPixels);
        std::wcout << L"Total Pixels=" << std::setw(8) << std::right << numPixels 
                   << L", Average Pixel Value = " << avgPixel;
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }

    std::wcout<<std::endl;

//    testReadWriteReadOnlyRunning(H, L"During Acquisition");
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}


