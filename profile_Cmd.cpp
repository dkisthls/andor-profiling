
#include "atcore.h"

#include "CmdLineUtil.h"
#include "constants.h"
#include "Timer.h" 
#include "utility.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <thread>
#include <algorithm>
#include <memory>
#include <ostream>
#include <sstream>

static const int SAMPLESTOTAKE = 500;

bool verbose(true);
bool debugEnabled(false);                               // Overridden by --debug
AT_BOOL directQueueing(DEFAULT_DIRECTQUEUEING);         // Overridden by --directQueueing

const std::vector<Bin> Bins = {
  Bin(1,1), Bin(2,2), Bin(3,3), Bin(4,4), Bin(8,8)
};
std::vector<int> uniqueGainModes;          // From command line option --gainMode
std::vector<std::string> gainModesToTest;  // The actual gainModes to run
std::vector<int> uniqueShutterModes;       // From command line option --shutterMode
std::vector<std::string> shutterModesToTest;  // The actual shutterModes to run


int AcquireFrames(AT_H H, int frameCount);
int AcquisitionLoop(AT_H H, int frameCount);
int Run( const CamInfo& camInfo );
int profileCmd( const CamInfo& camInfo, 
                const std::string& shutterMode, const std::string& what );

//--------------------------------- usage() ------------------------------------
//
void usage(char *progname)
{
  std::cout.precision(6);
  std::cout << std::endl
    << "Profiles time it takes to configure the camera for binning." << std::endl << std::endl
    << "Usage: " << progname << " [options]" << std::endl
    << "     --help" << std::endl
    << "       This usage help" << std::endl
    << std::endl
    << "     --debug" << std::endl
    << "       Enable debug output." << std::endl
    << std::endl
    << "     --disableDirectQueueing=<bool>\n"
    << "       Direct queueing is enabled by default. Use this flag to disable it.\n"
    << std::endl
    << "     --gainMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorGainModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaGainModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaGainModes[i] << std::endl;
  std::cout << "       (default=All gainModes of camera)\n"
    << std::endl
    << "     --shutterMode=<int>{,<int>}\n"
    << "       Used to specify the gain mode(s) to use in conjunction with\n"
    << "       this test (default=All gainModes of camera). <int> is one of:\n"
    << "         Andor Balor Camera:\n";
  for( int i=0; i<balorShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << balorShutterModes[i] << std::endl;
  std::cout << "         Andor Zyla Camera:\n";
  for( int i=0; i<zylaShutterModes.size(); i++ )
    std::cout << "           " << i+1 << ": " << zylaShutterModes[i] << std::endl;
  std::cout << "       (default=All shutterModes of camera)\n"
    << std::endl;
}
// End of usage()


//------------------------------ cmdLineError() --------------------------------
//
void cmdLineError(char* progname, std::ostringstream& errMsg)
{
  std::cout << std::endl <<  "*** Error - " << errMsg.str() << " ***" << std::endl;
  usage(progname);
  exit(1);
}
// End of cmdLineError()


//--------------------------------- main() -------------------------------------
//
int main(int argc, char* argv[])
{
  int equalsSign;
  int cmdSwitchPos;
  char *progName(argv[0]);
  std::ostringstream errMsg;

  argc--;
  argv++;
  try {
    while( argc )
    {
      if( argv[0][0] == '-' && argv[0][1] == '-' )
      {
        std::string option(&argv[0][2]);
        if( option == "help" )
        {
          usage(progName);
          return 1;
        }
        else if( option == "debug" )
        {
          debugEnabled = true;
        }
        else if( option == "disableDirectQueueing" )
        {
          directQueueing = AT_FALSE;
        }
        else if( (cmdSwitchPos = option.find("gainMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxGainModes = std::max(balorGainModes.size(), zylaGainModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueGainModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxGainModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else if( (cmdSwitchPos = option.find("shutterMode")) != std::string::npos &&
                 cmdSwitchPos == 0 )
        {
          int maxShutterModes = std::max(balorShutterModes.size(), zylaShutterModes.size() );
          if( (equalsSign = option.find_last_of("=")) != std::string::npos )
          {
            std::string values = option.substr(equalsSign);
            uniqueShutterModes = CmdLineUtil::getIntChoices(&argv[0][0], values, maxShutterModes);
          }
          else
            throw std::invalid_argument("Invalid or missing value(s)");
        }
        else
        {
          errMsg << "Unknown command line opiton: " << &argv[0][0];
          cmdLineError( progName, errMsg );
        }
      }

      argc--;
      argv++;
    }
  } catch( std::out_of_range& ex ) {
    errMsg << ex.what() << ": " << &argv[0][0];
    cmdLineError( progName, errMsg );
  } catch( std::invalid_argument& ex ) {
    errMsg << "Invalid or missing value in option " << &argv[0][0];
    cmdLineError( progName, errMsg );
  }

  CamInfo camInfo;
  RETURN_ON_ERROR( OpenAndIdentify( camInfo ) );

  if( !uniqueGainModes.empty() )
  {
    for( int v : uniqueGainModes )
    {
      if( v <= 0 || v > camInfo.getGainModes().size() )
      {
        errMsg << "invalid selected gainMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        gainModesToTest.push_back( camInfo.getGainModes()[v-1] );
    }
  }
  else
    gainModesToTest = camInfo.getGainModes();

  if( !uniqueShutterModes.empty() )
  {
    for( int v : uniqueShutterModes )
    {
      if( v <= 0 || v > camInfo.getShutterModes().size() )
      {
        errMsg << "invalid selected shutterMode";
        CmdLineUtil::cmdLineError( progName, errMsg );
      }
      else
        shutterModesToTest.push_back( camInfo.getShutterModes()[v-1] );
    }
  }
  else
    shutterModesToTest = camInfo.getShutterModes();

  // Summarize test setup and command line options...
  std::cout << "General Test Parameters:" << std::endl
            << "  debugEnabled=" << std::boolalpha << debugEnabled << std::endl
            << "  directQueueing=" << std::boolalpha << ((directQueueing == AT_TRUE) ? true : false) << std::endl;

  std::cout << "  gainModesToTest=";
  for( int i=0; i<gainModesToTest.size()-1; i++ )
    std::cout << "\"" << gainModesToTest[i] << "\",";
  std::cout << "\"" << gainModesToTest[gainModesToTest.size()-1] << "\"" << std::endl;

  std::cout << "  shutterModesToTest=";
  for( int i=0; i<shutterModesToTest.size()-1; i++ )
    std::cout << "\"" << shutterModesToTest[i] << "\",";
  std::cout << "\"" << shutterModesToTest[shutterModesToTest.size()-1] << "\"" << std::endl;

  int err = Run( camInfo );

  if (PAUSEAFTER) {
    std::cout << "Press enter to exit ..." << std::endl;
    getchar();
  }

  WARN_ON_ERROR(AT_Close(camInfo.getHandle()));
  WARN_ON_ERROR(AT_FinaliseLibrary());

  return err;
}
// End of main()


//---------------------------------- Run() -------------------------------------
//
int Run( const CamInfo& camInfo ) 
{
  int err = AT_SUCCESS;
  int passNumber(0);
  Timer totalTestDuration;
  Timer testDuration;
  AT_H H(camInfo.getHandle());
  const Bin binningOff(1, 1);

  totalTestDuration.start();

  for( std::string shutterMode : shutterModesToTest )
  {
    for( std::string gainMode : gainModesToTest )
    {
      passNumber++;
      std::string passDescription = "PASS #" + std::to_string(passNumber) 
        + ": ElectronicShutteringMode=" + shutterMode + ", GainMode=" + gainMode;


      std::cout << std::endl;
      std::cout << "====================================================================================================" 
                << std::endl;
      std::cout << passDescription << std::endl;
      auto timenow =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
      std::cout << "Date: " << std::ctime(&timenow);

      RETURN_ON_ERROR( SetBasicConfiguration(camInfo));
      RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"ElectronicShutteringMode", 
                                                 stringToWcs(shutterMode).c_str()) );

      if( camInfo.getCameraId() == BALOR )
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"GainMode", 
                                                   stringToWcs(gainMode).c_str()) );
      }
      else
      {
        RETURN_ON_ERROR( PrintAndSetEnumFeature(H, L"SimplePreAmpGainControl",
                                                   stringToWcs(gainMode).c_str()) );
      }
      
      for( Bin bin: Bins )
      {
        std::cout << std::endl
                  << "====================================================================================================" 
                  << std::endl
                  << "Samples per test: " << SAMPLESTOTAKE << std::endl;

        // On binning tests Left/Top don't change.
        // AOI width/height are in super-pixels.
        AOI aoi(camInfo.getFullFrameAOI().left,
                camInfo.getFullFrameAOI().top,
                camInfo.getFullFrameAOI().width / bin.x, 
                camInfo.getFullFrameAOI().height / bin.y,
                camInfo.getFullFrameAOI().desc);

        std::cout << "Binning Size: " << bin << std::endl 
                  << "AOI Size: " << aoi << std::endl << std::endl;

        // Reset to full frame, no binning
        RETURN_ON_ERROR( AT_SetInt(H, L"AOIHBin",   binningOff.x) );
        RETURN_ON_ERROR( AT_SetInt(H, L"AOIVBin",   binningOff.y) );
        RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  camInfo.getFullFrameAOI().width) );
        RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   camInfo.getFullFrameAOI().left) );
        RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", camInfo.getFullFrameAOI().height) );
        RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    camInfo.getFullFrameAOI().top) );

        // We just reset to full frame, no binning. Don't set them again
        // if we're going back to full frame, no binning
        if( bin.x != 1 && bin.y != 1 )
        {
          // Apply new binning and AOI(in super-pixels).
          RETURN_ON_ERROR( AT_SetInt(H, L"AOIHBin",   bin.x) );
          RETURN_ON_ERROR( AT_SetInt(H, L"AOIVBin",   bin.y) );
          RETURN_ON_ERROR( AT_SetInt(H, L"AOIWidth",  aoi.width) );
          RETURN_ON_ERROR( AT_SetInt(H, L"AOILeft",   aoi.left) );
          RETURN_ON_ERROR( AT_SetInt(H, L"AOIHeight", aoi.height) );
          RETURN_ON_ERROR( AT_SetInt(H, L"AOITop",    aoi.top) );
        }

        std::string what;
        what = "Width";
        testDuration.start();
        err = profileCmd( camInfo, shutterMode, what );
        testDuration.stop();
      
        if(AT_SUCCESS==err) 
        {
          std::cout.precision(2);
          std::cout << "Completed successfully, test duration=" 
                    << std::fixed << std::setprecision(2)
                    << (testDuration.get_dt() / 60.0 ) << "min!" << std::endl;
        }
        else 
          std::cout << "Failed!" << std::endl;

      }
    }
  }

  std::cout << std::endl
            << "====================================================================================================" 
            << std::endl;

  totalTestDuration.stop();
  std::cout.precision(2);
  std::cout << "Testing complete, Total Testing Duration=" 
            << std::fixed << std::setprecision(2)
            << (totalTestDuration.get_dt() / 60.0 ) << "min!" << std::endl << std::endl;

  return err;
}
// End of Run()


int acquireCount=0;

static const double RANGE_1_MAX = 0.1;
static const double RANGE_2_MAX = 1.0;
static const double RANGE_3_MAX = 10.0;
static const double RANGE_4_MAX = 30.0;

static const double RANGE_1_INC = 0.00005;
static const double RANGE_2_INC = 0.001;
static const double RANGE_3_INC = 0.010;
static const double RANGE_4_INC = 0.100;
static const double RANGE_5_INC = 1.0;


//------------------------------ profileCmd() ----------------------------------
//
int profileCmd( 
  const CamInfo& camInfo, 
  const std::string& shutterMode, 
  const std::string& what )
{
  bool cameraAcquiring(false);
  AT_H H(camInfo.getHandle());
  std::string acqStateForTest;
  std::ostringstream msgStr;
  std::vector<image_t> frameBuffers;

  double rowReadTime;
  RETURN_ON_ERROR( AT_GetFloat(H, L"RowReadTime", &rowReadTime) );

  // Get exposure time limits. 
  // Since acquisition is disabled, this will be the camera limits.
  double expTimeLimits[2];
  RETURN_ON_ERROR( AT_GetFloatMin(H, L"ExposureTime", &expTimeLimits[IDX_MIN]) );
  RETURN_ON_ERROR( AT_GetFloatMax(H, L"ExposureTime", &expTimeLimits[IDX_MAX]) );

  std::string pad1 = std::string(44, ' ');
  std::cout << pad1 << "  Min   ,   Max   ,   Avg   " << std::endl << std::flush; 

  //---------------- Get LongExposureTransition
  if( camInfo.getCameraId() == BALOR )
  {
    double longExposureTransition;
    for( int i=0; i<2; i++ )
    {
      double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
      Timer tTotal;
      Timer tConfig;
      
      if( i == 0 )
      {
        if( cameraAcquiring )
        {
          RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
          RETURN_ON_ERROR(AT_Flush(H));
          cameraAcquiring = false;
        }
        acqStateForTest = "disabled";
      }
      else
      {
        AllocateAndQueueBuffers( H, frameBuffers );
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
        cameraAcquiring = true;
        acqStateForTest = "enabled";
      }

      for( int j=0; j<SAMPLESTOTAKE; j++ )
      {
        std::cout << "Get \"LongExposureTransition\""
                  << ": Taking Sample " << j+1 << " of " << SAMPLESTOTAKE << "\r"
                  << std::flush;

        tConfig.start();
        RETURN_ON_ERROR( AT_GetFloat(H, L"LongExposureTransition", &longExposureTransition) );
        tConfig.stop();

        double dtConfig( tConfig.get_dt() );
        minConfig = std::min( minConfig, dtConfig );
        maxConfig = std::max( maxConfig, dtConfig );
        sumConfig += dtConfig; 
      }

      if( cameraAcquiring )
      {
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
        RETURN_ON_ERROR(AT_Flush(H));
        cameraAcquiring = false;
      }

      double avgConfig = sumConfig / static_cast<double>(SAMPLESTOTAKE); 

      msgStr.str("");
      msgStr.precision(6);
      std::string pad2 = std::string(5, ' ');
      msgStr << "Get \"LongExposureTransition\"=" 
             << std::fixed << longExposureTransition << "s:" << pad2 
             << std::fixed << minConfig << ", " << std::fixed << maxConfig << ", " 
             << std::fixed << avgConfig 
             << " (acquisition " << acqStateForTest << ")" << std::endl;
      std::cout << msgStr.str() << std::flush;
    }  
  }

  //---------------- Get ExposureTime
  // Test getting exposure time.
  // Test twice: Once with acquisition disabled and once with acquisiton enabled
  for( int i=0; i<2; i++ )
  {
    double exposureTime;
    double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
    Timer tConfig;
   
    if( i == 0 )
    {
      if( cameraAcquiring )
      {
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
        RETURN_ON_ERROR(AT_Flush(H));
        cameraAcquiring = false;
      }
      acqStateForTest = "disabled";
    }
    else
    {
      AllocateAndQueueBuffers( H, frameBuffers );
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
      cameraAcquiring = true;
      acqStateForTest = "enabled";
    }

// RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", 0.015) ); // Initial value
// RETURN_ON_ERROR( PrintFloatFeatureMinMax(H, L"ExposureTime") );
// RETURN_ON_ERROR( PrintFloatFeature(H, L"ReadoutTime") );
// rowReadTime = 0.00000923943661972;
// std::cout << "Calulated Min: " << std::fixed << std::setprecision(9) 
//           << rowReadTime << std::endl; 
// std::cout << "Calulated RowReadtime: " << std::fixed << std::setprecision(17) 
//           << (2624.0 * (1.0/280000000.0)) << std::endl;
// std::cout << "Calulated Readout: " << std::fixed << std::setprecision(9) 
//           << (1080.0 * rowReadTime) << " (short)" << std::endl;
// std::cout << "Calulated Max: " << std::fixed << std::setprecision(9) 
//           << (1080.0 * rowReadTime) + (3.0 * rowReadTime) << " (short)" << std::endl;
// std::cout << "Calulated Max: " << std::fixed << std::setprecision(9) 
//           << (1080.0 * rowReadTime) + (4.0 * rowReadTime) << " (long)" << std::endl;

    for( int j=0; j<SAMPLESTOTAKE; j++ )
    {
      std::cout << "Get \"ExposureTime\": Taking Sample " 
                << j+1 << " of " << SAMPLESTOTAKE << '\r' << std::flush;

      tConfig.start();
      RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &exposureTime) );
      tConfig.stop();

      double dtConfig( tConfig.get_dt() );
      minConfig = std::min( minConfig, dtConfig );
      maxConfig = std::max( maxConfig, dtConfig );
      sumConfig += dtConfig; 
    }

    if( cameraAcquiring )
    {
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
      RETURN_ON_ERROR(AT_Flush(H));
      cameraAcquiring = false;
    }

    double avgConfig = sumConfig / static_cast<double>(SAMPLESTOTAKE); 

    msgStr.str("");
    msgStr.precision(6);
    std::string pad2 = std::string(15, ' ');
    msgStr << "Get \"ExposureTime\"=" 
           << std::fixed << exposureTime << "s:" << pad2 
           << std::fixed << minConfig << ", " << std::fixed << maxConfig << ", " 
           << std::fixed << avgConfig 
           << " (acquisition " << acqStateForTest << ")" << std::endl;
    std::cout << msgStr.str() << std::flush;
  }  

  //---------------- Set ExposureTime w/LongExposureTransition crossings
  // Pingpong back and forth across the long exposure transition boundary using 
  // the cameras exposure time limits.
  // Acquistion Disabled
  {
    double minConfig[]={10.0,10.0}, maxConfig[]={0.0,0.0}, sumConfig[]={0.0,0.0};
    double avgConfig[]={0.0,0.0};
    Timer tConfig;
    
    RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", expTimeLimits[IDX_MAX]) );
  
    for( int i=0; i<SAMPLESTOTAKE; i++ )
    {
      for( int j=0; j<2; j++ )
      {
        std::cout << "Set \"ExposureTime\" " 
                  << std::setprecision(6) << std::fixed << expTimeLimits[IDX_MIN] << "s"
                  << (j==0 ? " <-- " : " --> ")
                  << std::setprecision(1) << std::fixed << expTimeLimits[IDX_MAX] << "s"
                  << ": Taking Sample " << i+1 << " of " << SAMPLESTOTAKE << '\r' 
                  << std::flush;

        tConfig.start();
        RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", expTimeLimits[j]) );
        tConfig.stop();

        double dtConfig( tConfig.get_dt() );
        minConfig[j] = std::min( minConfig[j], dtConfig );
        maxConfig[j] = std::max( maxConfig[j], dtConfig );
        sumConfig[j] += dtConfig; 
      }
    }

    msgStr.str("");
    std::string pad2 = std::string(4, ' ');
    for( int j=0; j<2; j++ )
    {
      avgConfig[j] = sumConfig[j] / static_cast<double>(SAMPLESTOTAKE); 

      msgStr << "Set \"ExposureTime\" " 
             << std::setprecision(6) << std::fixed << expTimeLimits[IDX_MIN] << "s"
             << (j==0 ? " <-- " : " --> ")
             << std::setprecision(1) << std::fixed << expTimeLimits[IDX_MAX] << "s:"
             << pad2;
      msgStr.precision(6);
      msgStr << std::fixed << minConfig[j] << ", " << std::fixed << maxConfig[j] << ", " 
             << std::fixed << avgConfig[j] 
             << (j==0 ? " (long to short crossings only)" : 
                        " (short to long crossings only)") << std::endl;
    }
    std::cout << msgStr.str() << std::flush;
  }

  //---------------- Set ExposureTime (< LongExposureTransition)
  // Test setting exposure times all on the short side of the transition.
  // Test twice: Once with acquisition disabled and once with acquisition enabled
  {
    int rowsRead;
    double longExposureTransition;
    double maxExpTime(0.0);
    RETURN_ON_ERROR( GetLongExposureTransition(
      camInfo, debugEnabled, camInfo.getFullFrameAOI(), longExposureTransition, rowsRead) );

    for( int i=0; i<2; i++ )
    {
      double currExpTime( expTimeLimits[IDX_MIN] + 0.00001 );    // iniitalize to small value
      double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
      Timer tConfig;

      RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", currExpTime) ); // Initial value

      if( i == 0 )
      {
        if( cameraAcquiring )
        {
          RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
          RETURN_ON_ERROR(AT_Flush(H));
          cameraAcquiring = false;
        }
        acqStateForTest = "disabled";
      }
      else
      {
        AllocateAndQueueBuffers( H, frameBuffers );
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
        cameraAcquiring = true;
        acqStateForTest = "enabled";
      }

      double newExpTime( expTimeLimits[IDX_MIN]);
      for( int j=0; j<SAMPLESTOTAKE; j++ )
      {
        std::cout << "Set \"ExposureTime\" " 
                  << std::setprecision(6) << std::fixed << currExpTime << "s"
                  << " --> "
                  << std::setprecision(6) << std::fixed << newExpTime << "s"
                  << ": Taking Sample " << j+1 << " of " << SAMPLESTOTAKE << '\r' 
                  << std::flush;

        tConfig.start();
        RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", newExpTime) );
        tConfig.stop();

        currExpTime = newExpTime;
        newExpTime = currExpTime + 0.00001;
        if( (currExpTime < longExposureTransition &&
             newExpTime >= (longExposureTransition - rowReadTime)) )
        {
          newExpTime = expTimeLimits[IDX_MIN];
        }
        maxExpTime = std::max(currExpTime, newExpTime);

        double dtConfig( tConfig.get_dt() );
        minConfig = std::min( minConfig, dtConfig );
        maxConfig = std::max( maxConfig, dtConfig );
        sumConfig += dtConfig; 
      }

      if( cameraAcquiring )
      {
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
        RETURN_ON_ERROR(AT_Flush(H));
        cameraAcquiring = false;
      }

      double avgConfig = sumConfig / static_cast<double>(SAMPLESTOTAKE); 

      msgStr.str("");
      msgStr.precision(6);
      msgStr << "Set \"ExposureTime\" " 
             << std::fixed << expTimeLimits[IDX_MIN] << "s"
             << " --> "
             << std::fixed << maxExpTime << "s: " //(longExposureTransition - rowReadTime) << "s: "
             << std::fixed << minConfig << ", " << std::fixed << maxConfig << ", " 
             << std::fixed << avgConfig 
             << " (acquisition " << acqStateForTest 
             << ", short exposure)" << std::endl;
      std::cout << msgStr.str() << std::flush;
    }
  }

  //---------------- Set ExposureTime (> LongExposureTransition)
  // Test setting exposure times all on the long side of the transition.
  // Test twice: Once with acquisition disabled and once with acquisiton enabled
  {
    int rowsRead;
    double longExposureTransition;
    double expInc( (shutterMode == "Global") ? 0.001 : 1.0 );
    RETURN_ON_ERROR( 
      GetLongExposureTransition(
        camInfo, debugEnabled, camInfo.getFullFrameAOI(), longExposureTransition, rowsRead) );

    double minExpTime( (shutterMode == "Global") ? longExposureTransition : 
                                                   expTimeLimits[IDX_MIN] );
    double maxExpTime(0.0);

    for( int i=0; i<2; i++ )
    {
      double currExpTime( minExpTime );
      double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
      Timer tConfig;

      RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", currExpTime) ); // Initial value

      if( i == 0 )
      {
        if( cameraAcquiring )
        {
          RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
          RETURN_ON_ERROR(AT_Flush(H));
          cameraAcquiring = false;
        }
        acqStateForTest = "disabled";
      }
      else
      {
        AllocateAndQueueBuffers( H, frameBuffers );
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
        cameraAcquiring = true;
        acqStateForTest = "enabled";
      }

      double newExpTime( (shutterMode == "Global") ? longExposureTransition : 
                                                     expTimeLimits[IDX_MIN] );;
      for( int j=0; j<SAMPLESTOTAKE; j++ )
      {
        std::cout << "Set \"ExposureTime\" " 
                  << std::setprecision(6) << std::fixed << currExpTime << "s"
                  << " --> "
                  << std::setprecision(6) << std::fixed << newExpTime << "s"
                  << ": Taking Sample " << j+1 << " of " << SAMPLESTOTAKE << '\r' 
                  << std::flush;

        tConfig.start();
        RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", newExpTime) );
        tConfig.stop();

        currExpTime = newExpTime;
        newExpTime = currExpTime + expInc;;
        if( currExpTime > expTimeLimits[IDX_MAX] )
        {
          newExpTime = longExposureTransition;
        }
        maxExpTime = std::max(currExpTime, newExpTime);

        double dtConfig( tConfig.get_dt() );
        minConfig = std::min( minConfig, dtConfig );
        maxConfig = std::max( maxConfig, dtConfig );
        sumConfig += dtConfig; 
      }

      if( cameraAcquiring )
      {
        RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
        RETURN_ON_ERROR(AT_Flush(H));
        cameraAcquiring = false;
      }

      double avgConfig = sumConfig / static_cast<double>(SAMPLESTOTAKE); 

      msgStr.str("");
      msgStr.precision(6);
      msgStr << "Set \"ExposureTime\" " << std::fixed << minExpTime << "s"
             << " --> "
             << std::fixed << maxExpTime << "s: "
             << std::fixed << minConfig << ", " << std::fixed << maxConfig << ", " 
             << std::fixed << avgConfig
             << " (acquisition " << acqStateForTest 
             << ", long exposure)" << std::endl;
      std::cout << msgStr.str() << std::flush;
    }
  }

/*

  // Using different increment values based on exposure time, display them now.
  msgStr << "Exposure Time: min=" << std::fixed << std::setprecision(6) << expMin
         << ", max=" << std::fixed << std::setprecision(6) << expMax << std::endl
         << "   Test Range 1: " 
         << std::setprecision(6) << std::fixed << std::setw(9) << expMin << " <= expTime < " 
         << std::setprecision(1) << std::fixed << RANGE_1_MAX << ",   increment: " 
         << std::setprecision(5) << std::fixed << RANGE_1_INC << std::endl
         << "   Test Range 2: " 
         << std::setprecision(6) << std::fixed << std::setw(9) << RANGE_1_MAX << " <= expTime < " 
         << std::setprecision(1) << std::fixed << RANGE_2_MAX << ",   increment: " 
         << std::setprecision(5) << std::fixed << RANGE_2_INC << std::endl
         << "   Test Range 3: " 
         << std::setprecision(6) << std::fixed << std::setw(9) << RANGE_2_MAX << " <= expTime < " 
         << std::setprecision(1) << std::fixed << RANGE_3_MAX << ",  increment: " 
         << std::setprecision(5) << std::fixed << RANGE_3_INC << std::endl
         << "   Test Range 4: " 
         << std::setprecision(6) << std::fixed << std::setw(9) << RANGE_3_MAX << " <= expTime < " 
         << std::setprecision(1) << std::fixed << RANGE_4_MAX << ",  increment: " 
         << std::setprecision(5) << std::fixed << RANGE_4_INC << std::endl
         << "   Test Range 5: " 
         << std::setprecision(6) << std::fixed << std::setw(9) << RANGE_4_MAX << " <= expTime < " 
         << std::setprecision(1) << std::fixed << expMax << ", increment: " 
         << std::setprecision(5) << std::fixed << RANGE_5_INC;
  std::cout << msgStr.str() << std::endl << std::endl;

  // Start acquistion so that in the measurement loop we will be measuring the time
  // to stop, set features, and start.
  AllocateAndQueueBuffers( H, frameBuffers );
  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  Timer tTotal;
  Timer tStop;
  Timer tFlush;
  Timer tConfig;
  Timer tQueue;
  Timer tStart;
  double minStop(10.0),   maxStop(0.0),   sumStop(0.0);
  double minFlush(10.0),  maxFlush(0.0),  sumFlush(0.0);
//  double minConfig(10.0), maxConfig(0.0), sumConfig(0.0);
  double minQueue(10.0),  maxQueue(0.0),  sumQueue(0.0);
  double minStart(10.0),  maxStart(0.0),  sumStart(0.0);
  double minTotal(10.0),  maxTotal(0.0),  sumTotal(0.0);

  msgStr.str("");
  msgStr << "Test,  expTime  ,   Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  ,  Readout, FrameRate " 
         << std::endl;
  std::cout << msgStr.str() << std::flush;

  int samples(0);
  double newExpTime(expMin);
  double currExpTime;
  double longExposureTransition;
  do
  {
    samples++;

    RETURN_ON_ERROR( AT_GetFloat(H, L"ExposureTime", &currExpTime) );
    RETURN_ON_ERROR( AT_GetFloat(H, L"LongExposureTransition", &longExposureTransition) );

    tTotal.start();

    bool stopAcquisitionToChangeExpTime(false);
    if( (currExpTime < longExposureTransition &&
         newExpTime >= longExposureTransition) ||
        (currExpTime >= longExposureTransition &&
         newExpTime < longExposureTransition) )
    {
      stopAcquisitionToChangeExpTime = true;
    }

    if( stopAcquisitionToChangeExpTime ) 
    {
      tStop.start();
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
      tStop.stop();

      tFlush.start();
      RETURN_ON_ERROR(AT_Flush(H));
      frameBuffers.clear();
      tFlush.stop();
    }

    tConfig.start();
    RETURN_ON_ERROR( AT_SetFloat(H, L"ExposureTime", newExpTime) );
    tConfig.stop();

    if( stopAcquisitionToChangeExpTime )
    {
      tQueue.start();
      AllocateAndQueueBuffers( H, frameBuffers );
      tQueue.stop();

      tStart.start();
      RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));
      tStart.stop();
    }

    tTotal.stop();

    double readoutTime;
    RETURN_ON_ERROR( AT_GetFloat(H, L"ReadoutTime", &readoutTime) );
    double frameRate;
    RETURN_ON_ERROR( AT_GetFloat(H, L"FrameRate", &frameRate) );

    double dtStop  = (stopAcquisitionToChangeExpTime) ? tStop.get_dt() : 0.0;
    double dtFlush = (stopAcquisitionToChangeExpTime) ? tFlush.get_dt() : 0.0;
    double dtConfig( tConfig.get_dt() );
    double dtQueue = (stopAcquisitionToChangeExpTime) ? tQueue.get_dt() : 0.0;
    double dtStart = (stopAcquisitionToChangeExpTime) ? tStart.get_dt() : 0.0;
    double dtTotal( tTotal.get_dt() );
    sumStop   += dtStop;
    sumFlush  += dtFlush;
    sumConfig += dtConfig;
    sumQueue  += dtQueue;
    sumStart  += dtStart;
    sumTotal  += dtTotal;
    minStop   = std::min( minStop, dtStop );
    maxStop   = std::max( maxStop, dtStop );
    minFlush  = std::min( minFlush, dtFlush );
    maxFlush  = std::max( maxFlush, dtFlush );
    minConfig = std::min( minConfig, dtConfig );
    maxConfig = std::max( maxConfig, dtConfig );
    minQueue  = std::min( minQueue, dtQueue );
    maxQueue  = std::max( maxQueue, dtQueue );
    minStart  = std::min( minStart, dtStart );
    maxStart  = std::max( maxStart, dtStart );
    minTotal  = std::min( minTotal, dtTotal );
    maxTotal  = std::max( maxTotal, dtTotal );

    msgStr.str("");
    msgStr.precision(6);
    msgStr << std::right << std::setw(4) << samples << ", "
           << std::right << std::setw(10) << newExpTime << ", "
           << std::fixed << dtStop << ", "
           << std::fixed << dtFlush << ", "
           << std::fixed << dtConfig << ", "
           << std::fixed << dtQueue << ", "
           << std::fixed << dtStart << ", "
           << std::fixed << dtTotal << ", "
           << std::fixed << readoutTime << ", "
           << std::fixed << frameRate
           << std::endl;
    std::cout << msgStr.str() << std::flush;

    if( newExpTime < RANGE_1_MAX ) 
      newExpTime += RANGE_1_INC;
    else if( newExpTime < RANGE_2_MAX ) 
      newExpTime += RANGE_2_INC;
    else if( newExpTime < RANGE_3_MAX ) 
      newExpTime += RANGE_3_INC;
    else if( newExpTime < RANGE_4_MAX ) 
      newExpTime += RANGE_4_INC;
    else 
      newExpTime += RANGE_5_INC;

  } while( newExpTime <= expMax );

  double avgStop(   sumStop / static_cast<double>(samples) ); 
  double avgFlush(  sumFlush / static_cast<double>(samples) ); 
  double avgConfig( sumConfig / static_cast<double>(samples) ); 
  double avgQueue(  sumQueue / static_cast<double>(samples) ); 
  double avgStart(  sumStart / static_cast<double>(samples) ); 
  double avgTotal(  sumTotal / static_cast<double>(samples) ); 

  std::string pad1 = std::string(18, ' ');
  std::string pad2 = std::string(12, ' ');

  msgStr.str("");
  msgStr.precision(6);
  msgStr 
    << pad1 << "  Stop  ,  Flush  ,  Config ,  Queue  ,  Start  ,  Total  " << std::endl
    << pad2 << "Min : " << std::fixed << minStop   << ", " << std::fixed << minFlush << ", " 
            << std::fixed << minConfig << ", " << std::fixed << minQueue << ", " 
            << std::fixed << minStart  << ", " << std::fixed << minTotal << std::endl
    << pad2 << "Max : " << std::fixed << maxStop   << ", " << std::fixed << maxFlush << ", " 
            << std::fixed << maxConfig << ", " << std::fixed << maxQueue << ", " 
            << std::fixed << maxStart  << ", " << std::fixed << maxTotal << std::endl
    << pad2 << "Avg : " << std::fixed << avgStop   << ", " << std::fixed << avgFlush << ", " 
            << std::fixed << avgConfig << ", " << std::fixed << avgQueue << ", " 
            << std::fixed << avgStart  << ", " << std::fixed << avgTotal << std::endl;
  std::cout << msgStr.str() << std::flush;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));
*/
  return AT_SUCCESS;
}
// End of profileCmd()


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int AcquireFrames(AT_H H, int frameCount) {
  AT_64 imageSizeBytes;

  RETURN_ON_ERROR(AT_GetInt(H, L"ImageSizeBytes", &imageSizeBytes));

  std::vector<image_t> images;
  for(int i=0; i<BUFFERCOUNT; i++)
  {
    image_t image(new AT_U8[imageSizeBytes]);
    int err = AT_QueueBuffer(H, image.get(), static_cast<int>(imageSizeBytes));
    if(err!=AT_SUCCESS) {
      std::wcout<<L"AT_QueueBuffer return code"<<err<<std::endl;
    }

    images.push_back(move(image));
  }


  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStart"));

  int err = AcquisitionLoop(H,frameCount);

  acquireCount++;

  RETURN_ON_ERROR(AT_Command(H, L"AcquisitionStop"));
  RETURN_ON_ERROR(AT_Flush(H));

  return err;
}

int AcquisitionLoop(AT_H H, int frameCount) {
  AT_64 aoiStride;
  RETURN_ON_ERROR(AT_GetInt(H,L"AOIStride",&aoiStride));

  auto start = std::chrono::steady_clock::now();
  int frame=0;
  while(true)
  {
    unsigned char *image_data;
    int image_size;
    int err = AT_WaitBuffer(H, &image_data, &image_size, TIMEOUTMS);

    if (err == AT_SUCCESS) {
      frame++;
/*      if(1==frame) {
        std::wcout<<L"Got first frame"<<std::endl;
      }
*/

      if (frame % UPDATEINTERVALFRAMES == 0) {
        auto end = std::chrono::steady_clock::now();
        auto diff = end - start;
        double duration = std::chrono::duration<double>(diff).count();
        double frameRate = static_cast<double>(frame) / duration;
//        std::wcout << L"Acquisition of " << frame << L" frames took " << duration << L"s, effective frame-rate " << frameRate << L"fps" << std::endl;

//        PrintMono16Frame(frame, image_data, image_size, static_cast<int>(aoiStride), PIXELSPERROWTOPRINT,ROWSTOPRINT);
        uint16_t* data16 = reinterpret_cast<uint16_t*>(image_data);
        double bytesPerPixel;
        RETURN_ON_ERROR( AT_GetFloat(H, L"BytesPerPixel", &bytesPerPixel) );

        double pixelSum(0);
        int numPixels = image_size / static_cast<int>(bytesPerPixel);
        for( int i=0; i<numPixels; i++)
        {
          pixelSum += data16[i];
        }
        double avgPixel = pixelSum / static_cast<double>(numPixels);
        std::wcout << L"Total Pixels=" << std::setw(8) << std::right << numPixels 
                   << L", Average Pixel Value = " << avgPixel;
      }
      if(frame>=frameCount) {
        break;
      }
      RETURN_ON_ERROR(AT_QueueBuffer(H, image_data, image_size))
    }
    else {
      std::wcout << L"AT_WaitBuffer when waiting for frame "<<(frame+1)<<L" return code: " << err << std::endl<<std::endl;
        return err;
    }

    std::wcout<<std::endl;

//    testReadWriteReadOnlyRunning(H, L"During Acquisition");
  }
  std::wcout << std::endl;
  return AT_SUCCESS;
}


